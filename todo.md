# Bug List

This is just a down and dirty list of bugs I can find as I'm using SST. This will help me keep track of them, and 
hopefully I will be able to knock them out for a solid 1.0 version.

* [ ] Environments
  * [X] 'Local' Mode
      * [X] When you edit an environment it says, 'You do not have permissions to edit System Environments.'
      * [X] When you try to save an environment with a duplicate slug, it doesn't give the user an error.
      * [X] When you try to save an environment with a duplicate slug, it throws a SQL error.
  * [X] 'Auth' Mode
      * [X] Users cannot have environments with the same slug.
      * [X] Users cannot change the slug, but it looks like you can.

## Fixes

* [ ] Project
  * [X] Add dotenv
  * [ ] Switch to strata-js/config-util
  * [X] Update Composition API
  * [ ] Update to Vue3
  * [ ] Update to Bootstrap 5 / BootstrapVueNext
  * [ ] Update code to match seed project
* [ ] Environments
  * [X] Remove 'slug'; use an auto-generated for every environment.
  * [ ] Wrap redis connections in a class that handles the reconnection logic correctly and gives feedback about if 
    the environment is 'up' or not.
* [X] Add a 'version' endpoint
* [ ] Add an 'about' modal
