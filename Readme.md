# Strata Service Tools

This is a UI for calling strata services. It supports service discovery, and in the future will add more tools than 
just making requests.

## Development

First, you'll want to create a `.env` file in the root of the project. This will be used to configure the 
application. For development, you can use the `.env.sample` file as a starting point. It will set you to use the 
local config, which is already set up for development.

Next, run `npm install` to install all dependencies, and then run:

* `npm run dev` - this runs the vite builder with hot module reloading

(If you want to run in 'production' mode, simply `npm run build` and then `npm start`.)

## Auth

__NOTE: If you're running this locally, you'll want to leave the auth section commented out to disable authentication.__

SST currently uses [GitLab][] as an identity provider, as it's easy to set up and where the code is hosted. (Also, 
we use this internally at the company that sponsors my development.)

If you don't want any authentication, just comment that out of your config file. If you want to use a different one, 
this currently isn't supported. In the future, I'd like to add a generic OAuth2 provider, as well as adding plugin 
support to allow any [passport.js][passport] strategies.

_Note: This does support on-premise instances of GitLab._

[passport]: http://www.passportjs.org/
[GitLab]: https://gitlab.com

### Configuring with Environment Variables

The following environment variables are used to configure the system. These can be set via a `.env` file, or via the 
normal environment variables.

| Env Var              |          Default          | Required | Description                                                                     |
|:--------------------:|:-------------------------:|:--------:|---------------------------------------------------------------------------------|
| SERVER_PORT          |          `9090`           | `false`  | The port the http and socket.io server runs on.                                 |
| SESSION_SECRET       |             -             | `true`   | The secret key for generating sessions. _Note: You should always specify this._ |
| GITLAB_URL           |  `'https://gitlab.com'`   | `false`  | The url to the gitlab instance for authentication.                              |
| GITLAB_CLIENT_ID     |             -             | `true`   | The client id for you app from gitlab.                                          |
| GITLAB_CLIENT_SECRET |             -             | `true`   | The client secret for your app from gitlab.                                     |
| CALLBACK_BASE_URL    | `'http://localhost:9000'` | `false`  | The base url of your running app, for the oauth callback.                       |

#### .env File

We have a`.env.sample` file in the root of the project. You can copy this to `.env` as a starting place.
