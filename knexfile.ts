// ---------------------------------------------------------------------------------------------------------------------
// Knex Migration configuration
//----------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------

import 'dotenv/config';
import configUtil from '@strata-js/util-config';
import type { Knex } from 'knex';

// Interfaces
import { ServerConfig } from './src/common/interfaces/config';

// Managers
import { getDB } from './src/server/utils/database';

//----------------------------------------------------------------------------------------------------------------------

const env = (process.env.ENVIRONMENT ?? 'local').toLowerCase();
configUtil.load(`./config/${ env }.yml`);

//----------------------------------------------------------------------------------------------------------------------

module.exports = async() =>
{
    const db = await getDB();

    // When this file is run, it expects the migrations to end in .ts, and we were changing the migrations to end in
    // .js. So, now we need to change them back.
    await db('knex_migrations')
        .select()
        .limit(1)
        .then(async() =>
        {
            await db.update({ name: db.raw('replace(name, \'.js\', \'.ts\')') })
                .from('knex_migrations');
        })
        .catch(async(error) =>
        {
            if(error.code !== 'SQLITE_ERROR')
            {
                throw error;
            } // end if
        });

    return {
        ...configUtil.get<ServerConfig>().database ?? {},
        migrations: {
            directory: './src/server/knex/migrations',
        },
        seeds: {
            directory: './src/server/knex/seeds',
        },
    } satisfies Knex.Config;
};

//----------------------------------------------------------------------------------------------------------------------
