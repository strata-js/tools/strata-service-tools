//----------------------------------------------------------------------------------------------------------------------
// Server Application
//----------------------------------------------------------------------------------------------------------------------

import http from 'http';
import path from 'path';
import express, { RequestHandler } from 'express';
import cookieParser from 'cookie-parser';
import session from 'express-session';
import passport from 'passport';
import helmet from 'helmet';
import { logging } from '@strata-js/strata';

import { Server as SIOServer } from 'socket.io';

// Interfaces
import { ServerConfig } from '../common/interfaces/config.js';

// Managers
import * as commandMan from './managers/command.js';
import * as permsMan from './managers/permissions.js';
import * as pluginMan from './managers/plugin.js';

// Session Store
import { ConnectSessionKnexStore } from 'connect-session-knex';

// Auth
import authMan from './auth/authentication.js';

// Routes
import { errorLogger, requestLogger, serveIndex } from './routes/utils/index.js';
import accountRouter from './routes/accounts.js';
import pluginRouter from './routes/plugin.js';
import rolesRouter from './routes/roles.js';
import environmentsRouter from './routes/environments.js';
import versionRouter from './routes/version.js';

// Version information
import { AddressInfo } from 'net';

// Utils
import * as dbUtil from './utils/database.js';
import { setSIOInstance, use as sioUse } from './utils/sio.js';
import { getVersion } from './utils/version.js';
import program from './utils/args.js';

//----------------------------------------------------------------------------------------------------------------------

const logger = logging.getLogger('sst-server');

//----------------------------------------------------------------------------------------------------------------------

export class SSTServer
{
    async start(config : ServerConfig) : Promise<void>
    {
        let devMode = false;
        if(program.args.includes('--dev'))
        {
            devMode = true;
        }

        // Get application version
        const version = await getVersion();

        // Build the express app
        const app = express();

        //--------------------------------------------------------------------------------------------------------------
        // Initialize managers
        //--------------------------------------------------------------------------------------------------------------

        await permsMan.init();
        await commandMan.init();
        await pluginMan.init(app);

        //--------------------------------------------------------------------------------------------------------------

        const store = new ConnectSessionKnexStore({
            sidFieldName: config.http.key,
            knex: await dbUtil.getDB() as any,
            createTable: true,

            // Clear expired sessions. (1 hour)
            cleanupInterval: 60 * 60 * 1000,
        });

        //--------------------------------------------------------------------------------------------------------------

        // Basic security fixes
        app.use(helmet({
            contentSecurityPolicy: false,
        }));

        // Basic request logging
        app.use(requestLogger() as RequestHandler);

        // Auth support
        app.use(cookieParser());
        app.use(express.json());

        app.use(session({
            secret: config.http.secret,
            key: config.http.key,
            resave: false,
            store,

            // maxAge = 7 days
            cookie: { maxAge: 7 * 24 * 60 * 60 * 1000, secure: config.http.secure },
            saveUninitialized: false,
        }));

        authMan.init(app);

        //--------------------------------------------------------------------------------------------------------------
        // Routing
        //--------------------------------------------------------------------------------------------------------------

        // Setup static serving
        app.use(express.static(path.resolve(import.meta.dirname, '..', '..', 'dist', 'client')));

        // Set up our application routes
        app.use('/api/accounts', accountRouter);
        app.use('/api/environments', environmentsRouter);
        app.use('/api/plugins', pluginRouter);
        app.use('/api/roles', rolesRouter);
        app.use('/version', versionRouter);

        // Serve index.html for any html requests, but 404 everything else.
        app.get('*', (_request, response) =>
        {
            response.format({
                html: serveIndex,
                json: (_req, resp) =>
                {
                    resp.status(404).end();
                },
            });
        });

        // Basic error logging
        app.use(errorLogger());

        //--------------------------------------------------------------------------------------------------------------
        // Server
        //--------------------------------------------------------------------------------------------------------------

        const server = http.createServer(app);

        const sio = new SIOServer(server, { maxHttpBufferSize: 1e7, pingTimeout: 30000 });

        // Send the sio server to the sio utility
        setSIOInstance(sio);

        // Setup sio middleware
        sioUse(session({
            secret: config.http.secret,
            key: config.http.key,
            resave: false,
            store,

            // maxAge = 7 days
            cookie: { maxAge: 7 * 24 * 60 * 60 * 1000, secure: config.http.secure },
            saveUninitialized: false,
        }));
        sioUse(passport.initialize());
        sioUse(passport.session());

        let httpPort = config.http.port;
        if(devMode)
        {
            httpPort -= 1;
        }

        // Start the server
        server.listen(httpPort, config.http.host, () =>
        {
            const { address, port } = server.address() as AddressInfo;
            const host = [ '::', '0.0.0.0' ].includes(address) ? 'localhost' : address;
            let actualPort = port;

            if(devMode)
            {
                logger.debug('Launching vite...');
                actualPort += 1;

                // Start Vite Dev Server
                (async() =>
                {
                    const { createServer } = await import('vite');
                    const viteServer = await createServer();
                    await viteServer.listen();
                })();
            }

            const url = `http://${ host }:${ actualPort }`;
            logger.info(`Strata Service Tools v${ version } listening at ${ url }`);
        });
    }
}

//----------------------------------------------------------------------------------------------------------------------
