//----------------------------------------------------------------------------------------------------------------------
// launcher.ts
//----------------------------------------------------------------------------------------------------------------------

import 'dotenv/config';

// Config
import configUtil from '@strata-js/util-config';
import { logging } from '@strata-js/strata';

import { ServerConfig } from '../common/interfaces/config.js';

configUtil.load(`./config/${ process.env.ENVIRONMENT }.yml`);
const config = configUtil.get<ServerConfig>();

// Logging
logging.setConfig(config.logging ?? { level: 'debug', prettyPrint: true });

//----------------------------------------------------------------------------------------------------------------------

const logger = logging.getLogger('global');

//----------------------------------------------------------------------------------------------------------------------
// Global Error Handler
//----------------------------------------------------------------------------------------------------------------------

process.on('uncaughtException', (err) =>
{
    logger.error(`Uncaught exception: ${ err.stack }`);
});

//----------------------------------------------------------------------------------------------------------------------
// Launch Server
//----------------------------------------------------------------------------------------------------------------------

// This has to be a dynamic import to make sure the config is loaded before anything else in the application can attempt
// to get it.
const { SSTServer } = await import('./server.js');
const server = new SSTServer();

try
{
    // Start the server
    await server.start(config);
}
catch (err)
{
    logger.error(`Error starting server: ${ err.stack }`);
    process.exit(1);
}

//----------------------------------------------------------------------------------------------------------------------
