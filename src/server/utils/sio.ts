//----------------------------------------------------------------------------------------------------------------------
// Socket.IO Handler Utility
//----------------------------------------------------------------------------------------------------------------------

import { Server, Socket } from 'socket.io';
import { NextFunction } from 'express';

import { MiddlewareFunction } from '../routes/utils/index.js';

// Managers
import * as commandMan from '../managers/command.js';
import * as envMan from '../managers/environment.js';

//----------------------------------------------------------------------------------------------------------------------

let sio : Server | undefined;

//----------------------------------------------------------------------------------------------------------------------

export interface SSTEnvelope
{
    type : 'update' | 'remove' | 'event';
    resource ?: string;
    payload ?: Record<string, unknown>;
}

//----------------------------------------------------------------------------------------------------------------------

function wrap(middleware : MiddlewareFunction)
{
    return (socket : Socket, next : NextFunction) : void => middleware(socket.request, {}, next);
}

//----------------------------------------------------------------------------------------------------------------------

export async function setSIOInstance(sioServer : Server) : Promise<void>
{
    sio = sioServer;
    sio.on('connection', (socket) =>
    {
        commandMan.register(socket);
        envMan.sendConnectionStatusUpdate(socket);
    });
}

export async function emitToAll(ev : string, message : any) : Promise<void>
{
    if(sio)
    {
        sio.emit(ev, message);
    }
}

export async function broadcast(namespace : string, message : SSTEnvelope) : Promise<void>
{
    if(sio)
    {
        const ns = sio.of(namespace);
        ns.emit('message', message);
    }
}

export function use(middleware : MiddlewareFunction) : void
{
    if(sio)
    {
        sio.use(wrap(middleware));
    }
}

//----------------------------------------------------------------------------------------------------------------------
