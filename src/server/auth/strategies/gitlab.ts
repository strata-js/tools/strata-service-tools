//----------------------------------------------------------------------------------------------------------------------
// GitLab Authentication Support
//----------------------------------------------------------------------------------------------------------------------

import configUtil from '@strata-js/util-config';
import { logging } from '@strata-js/strata';
import passport from 'passport';
import GitLabStrategy from 'passport-gitlab2';
import { Express } from 'express';

// Interfaces
import { ServerConfig } from '../../../common/interfaces/config.js';

// Managers
import * as accountMan from '../../managers/account.js';

//----------------------------------------------------------------------------------------------------------------------

const config = configUtil.get<ServerConfig>();
const logger = logging.getLogger('gitlab');

const CALLBACK_URL = process.argv.includes('--dev') ? 'http://localhost:9090' : 'http://localhost:9000';

//----------------------------------------------------------------------------------------------------------------------

passport.use(new GitLabStrategy(
    {
        baseURL: config.auth?.options.baseURL ?? 'https://gitlab.com',
        clientID: config.auth?.options.clientID ?? 'none',
        clientSecret: config.auth?.options.clientSecret ?? 'none',
        callbackURL: `${ config.auth?.options.callbackURL ?? CALLBACK_URL }/auth/gitlab/callback`,
    },
    async(_accessToken, _refreshToken, profile, done) =>
    {
        profile = profile._json;
        try
        {
            let account;
            try { account = await accountMan.getByEmail(profile.email); }
            catch (error)
            {
                if(error.code === 'ERR_NOT_FOUND')
                {
                    account = null;
                }
                else
                {
                    logger.error(`Encountered error during authentication:\n${ error.stack }`, error);
                    done(error);
                }
            }

            if(account)
            {
                // First, filter out 'admin' permissions, if any.
                const permissions = account.permissions.filter((perm) => perm === '*/*');

                // Next, if we're an admin, add.
                if(profile.is_admin)
                {
                    permissions.push('*/*');
                }

                account = await accountMan.update(account.id, {
                    name: profile.name,
                    avatar: profile.avatar_url,
                    permissions,
                });
            }
            else
            {
                account = await accountMan.add({
                    name: profile.name,
                    avatar: profile.avatar_url,
                    email: profile.email,
                    permissions: profile.is_admin ? [ '*/*' ] : [],
                });
            }

            done(null, account);
        }
        catch (error)
        {
            logger.error(`Encountered error during authentication:\n${ error.stack }`, error);
            done(error);
        }
    }
));

//----------------------------------------------------------------------------------------------------------------------

export default {
    initialize(app : Express) : void
    {
        app.get('/auth/gitlab', passport.authenticate('gitlab', { scope: [ 'read_api' ] }));

        app.get('/auth/gitlab/callback', passport.authenticate('gitlab', { scope: [ 'read_api' ] }), (_req, resp) =>
        {
            resp.redirect('/');
        });
    },
};

//----------------------------------------------------------------------------------------------------------------------
