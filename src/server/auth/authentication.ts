// ---------------------------------------------------------------------------------------------------------------------
// Authentication Manager
// ---------------------------------------------------------------------------------------------------------------------

import { Express } from 'express';
import passport from 'passport';
import configUtil from '@strata-js/util-config';

// Interfaces
import { ServerConfig } from '../../common/interfaces/config.js';

// Managers
import * as accountMan from '../managers/account.js';

// Models
import { Account } from '../models/account.js';

// Strategies
import GitlabStrategy from './strategies/gitlab.js';

// ---------------------------------------------------------------------------------------------------------------------

const strategies = {
    gitlab: GitlabStrategy,
};

// Set up passport
passport.serializeUser(({ id } : Account, done) => { done(null, id); });

passport.deserializeUser((id : number, done) =>
{
    accountMan.get(id)
        .then((account) => done(null, account))
        .catch((error) => done(error));
});

// ---------------------------------------------------------------------------------------------------------------------

class AuthenticationManager
{
    get strategy() : string | undefined
    {
        const config = configUtil.get<ServerConfig>();
        return config.auth?.strategy;
    }

    get authEnabled() : boolean
    {
        const config = configUtil.get<ServerConfig>();
        return !!(config.auth && config.auth.strategy && config.auth.options.clientID);
    }

    init(app : Express) : void
    {
        // Passport support
        app.use(passport.initialize());
        app.use(passport.session());

        // Authenticate
        app.get('/auth/user', (req, resp) =>
        {
            if(this.authEnabled)
            {
                resp.json(req.user);
            }
            else
            {
                // Use the mythical 418 status to indicate authentication is disabled.
                resp.status(418).end();
            }
        });

        // Logout endpoint
        app.post('/auth/logout', (req, res, next) =>
        {
            req.logout((err) =>
            {
                if(err)
                {
                    console.error('Failed to logout:', err.stack);
                    return next(err);
                }
                res.end();
            });

            res.end();
        });

        // Initialize the desired strategy.
        const strategy = strategies[this.strategy ?? 'none'];
        if(strategy)
        {
            // Initialize the Strategy
            strategy.initialize(app);
        }
    }
}

// ---------------------------------------------------------------------------------------------------------------------

export default new AuthenticationManager();

// ---------------------------------------------------------------------------------------------------------------------
