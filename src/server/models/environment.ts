//----------------------------------------------------------------------------------------------------------------------
// Environment
//----------------------------------------------------------------------------------------------------------------------

// Utils
import { shortID } from '../../common/utils/misc.js';

//----------------------------------------------------------------------------------------------------------------------

/* eslint-disable camelcase */

interface EnvironmentOptions
{
    id ?: string;
    name : string;
    host : string;
    port : number;
    db ?: number;
    backend : string;
    username ?: string;
    password ?: string;
    secure : boolean;
    accountID : number;
}

//----------------------------------------------------------------------------------------------------------------------

export class Environment
{
    public id : string;
    public name : string;
    public host : string;
    public port : number;
    public db : number;
    public backend : string;
    public username ?: string;
    public password ?: string;
    public secure : boolean;
    public accountID : number;

    constructor(options : EnvironmentOptions)
    {
        this.id = options.id ?? shortID();
        this.name = options.name;
        this.host = options.host;
        this.port = options.port;
        this.db = options.db ?? 0;
        this.backend = options.backend;
        this.username = options.username;
        this.password = options.password;
        this.secure = options.secure;
        this.accountID = options.accountID;
    }

    //------------------------------------------------------------------------------------------------------------------
    // Serialization
    //------------------------------------------------------------------------------------------------------------------

    public toJSON() : Record<string, unknown>
    {
        return {
            id: this.id,
            name: this.name,
            host: this.host,
            port: this.port,
            db: this.db,
            backend: this.backend,
            username: this.username,
            password: this.password,
            secure: this.secure,
            accountID: this.accountID,
        };
    }

    public toDB() : Record<string, unknown>
    {
        const { accountID, id, ...jsonObj } = this.toJSON();
        return {
            ...jsonObj,
            env_id: id,
            account_id: accountID,
        };
    }

    //------------------------------------------------------------------------------------------------------------------
    // Deserialization
    //------------------------------------------------------------------------------------------------------------------

    static fromDB(envRecord : Record<string, unknown>) : Environment
    {
        envRecord.secure = !!envRecord.secure;
        envRecord.db = envRecord.db ?? 0;

        // FIXME: This bypasses all the checks of TypeScript! We should use AJV to do the validation here.
        return new Environment(envRecord as unknown as EnvironmentOptions);
    }

    static fromJSON(jsonObj : Record<string, unknown>) : Environment
    {
        // FIXME: This bypasses all the checks of TypeScript! We should use AJV to do the validation here.
        return new Environment(jsonObj as unknown as EnvironmentOptions);
    }
}

//----------------------------------------------------------------------------------------------------------------------
