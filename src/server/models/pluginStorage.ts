//----------------------------------------------------------------------------------------------------------------------
// Plugin Storage
//----------------------------------------------------------------------------------------------------------------------

import { SSTPluginStorage, SSTPluginStorageConfig } from '../../common/interfaces/plugins.js';

//----------------------------------------------------------------------------------------------------------------------

/* eslint-disable camelcase */

export class PluginStorage implements SSTPluginStorage
{
    public readonly plugin : string;
    public readonly storage ?: SSTPluginStorageConfig;
    public readonly accountId ?: number;

    constructor(pluginStorage : SSTPluginStorage)
    {
        this.plugin = pluginStorage.plugin;
        this.storage = pluginStorage.storage;
        this.accountId = pluginStorage.accountId;
    }

    //------------------------------------------------------------------------------------------------------------------
    // Serialization
    //------------------------------------------------------------------------------------------------------------------

    public toJSON() : Record<string, unknown>
    {
        return {
            plugin: this.plugin,
            storage: this.storage,
            accountId: this.accountId,
        };
    }

    public toDB() : Record<string, unknown>
    {
        const { accountId, ...jsonObj } = this.toJSON();
        return {
            ...jsonObj,
            account_id: accountId,
            storage: JSON.stringify(this.storage),
        };
    }

    //------------------------------------------------------------------------------------------------------------------
    // Deserialization
    //------------------------------------------------------------------------------------------------------------------

    static fromDB(pluginStorageRecord : Record<string, unknown>) : PluginStorage
    {
        pluginStorageRecord.storage = JSON.parse(pluginStorageRecord.storage as string);

        // FIXME: This bypasses all the checks of TypeScript! We should use AJV to do the validation here.
        return new PluginStorage(pluginStorageRecord as unknown as PluginStorage);
    }

    static fromJSON(jsonObj : Record<string, unknown>) : PluginStorage
    {
        // FIXME: This bypasses all the checks of TypeScript! We should use AJV to do the validation here.
        return new PluginStorage(jsonObj as unknown as PluginStorage);
    }
}

//----------------------------------------------------------------------------------------------------------------------
