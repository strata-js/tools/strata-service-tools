//----------------------------------------------------------------------------------------------------------------------
// Account
//----------------------------------------------------------------------------------------------------------------------

import { AccountOptions, AccountSettings } from '../../common/interfaces/account.js';

//----------------------------------------------------------------------------------------------------------------------

/* eslint-disable camelcase */

export class Account
{
    public readonly id : number;
    public readonly email : string = '';

    public name = '';
    public avatar = '';
    public permissions : string[] = [];
    public groups : string[] = [];
    public settings : AccountSettings = {};

    constructor(options : AccountOptions)
    {
        this.id = options.id;
        this.email = options.email;
        this.name = options.name;
        this.avatar = options.avatar || '';
        this.permissions = options.permissions ?? [];
        this.groups = options.groups ?? [];
        this.settings = options.settings ?? {};
    }

    //------------------------------------------------------------------------------------------------------------------
    // Serialization
    //------------------------------------------------------------------------------------------------------------------

    public toJSON() : Record<string, unknown>
    {
        return {
            id: this.id,
            email: this.email,
            name: this.name,
            avatar: this.avatar,
            permissions: this.permissions,
            groups: this.groups,
            settings: this.settings,
        };
    }

    public toDB() : Record<string, unknown>
    {
        const { id, groups, ...jsonObj } = this.toJSON();
        return {
            ...jsonObj,
            account_id: id,
            permissions: JSON.stringify(this.permissions),
            settings: JSON.stringify(this.settings),
        };
    }

    //------------------------------------------------------------------------------------------------------------------
    // Deserialization
    //------------------------------------------------------------------------------------------------------------------

    static fromDB(accountRecord : Record<string, unknown>) : Account
    {
        accountRecord.permissions = JSON.parse(accountRecord.permissions as string);
        accountRecord.settings = JSON.parse(accountRecord.settings as string);

        // FIXME: This bypasses all the checks of TypeScript! We should use AJV to do the validation here.
        return new Account(accountRecord as unknown as AccountOptions);
    }

    static fromJSON(jsonObj : Record<string, unknown>) : Account
    {
        // FIXME: This bypasses all the checks of TypeScript! We should use AJV to do the validation here.
        return new Account(jsonObj as unknown as AccountOptions);
    }
}

//----------------------------------------------------------------------------------------------------------------------
