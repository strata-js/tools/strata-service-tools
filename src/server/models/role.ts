//----------------------------------------------------------------------------------------------------------------------
// Role
//----------------------------------------------------------------------------------------------------------------------

import { RoleOptions } from '../../common/interfaces/role.js';

//----------------------------------------------------------------------------------------------------------------------

/* eslint-disable camelcase */

export class Role
{
    public readonly id : number;
    public readonly name : string;
    public permissions : string[] = [];

    constructor(options : RoleOptions)
    {
        this.id = options.id;
        this.name = options.name;
        this.permissions = options.permissions ?? [];
    }

    //------------------------------------------------------------------------------------------------------------------
    // Serialization
    //------------------------------------------------------------------------------------------------------------------

    public toJSON() : Record<string, unknown>
    {
        return {
            id: this.id,
            name: this.name,
            permissions: this.permissions,
        };
    }

    public toDB() : Record<string, unknown>
    {
        const { id, ...jsonObj } = this.toJSON();
        return {
            ...jsonObj,
            role_id: id,
            permissions: JSON.stringify(this.permissions),
        };
    }

    //------------------------------------------------------------------------------------------------------------------
    // Deserialization
    //------------------------------------------------------------------------------------------------------------------

    static fromDB(roleRecord : Record<string, unknown>) : Role
    {
        roleRecord.permissions = JSON.parse(roleRecord.permissions as string);

        // FIXME: This bypasses all the checks of TypeScript! We should use AJV to do the validation here.
        return new Role(roleRecord as unknown as RoleOptions);
    }

    static fromJSON(jsonObj : Record<string, unknown>) : Role
    {
        // FIXME: This bypasses all the checks of TypeScript! We should use AJV to do the validation here.
        return new Role(jsonObj as unknown as RoleOptions);
    }
}

//----------------------------------------------------------------------------------------------------------------------
