// ---------------------------------------------------------------------------------------------------------------------
// Account ResourceAccess
// ---------------------------------------------------------------------------------------------------------------------

// Models
import { Account } from '../models/account.js';

// Interfaces
import { AccountFilters } from '../../common/interfaces/filters.js';

// Utils
import { getDB } from '../utils/database.js';

// Errors
import { MultipleResultsError, NotFoundError } from '../errors.js';

// ---------------------------------------------------------------------------------------------------------------------

/* eslint-disable camelcase */

export async function list(filters : AccountFilters) : Promise<Account[]>
{
    const db = await getDB();
    const query = db('account')
        .select(
            'account_id as id',
            'email',
            'name',
            'avatar',
            'permissions',
            'settings'
        );

    if(filters.id)
    {
        query.where({ account_id: filters.id });
    }

    if(filters.email)
    {
        query.where({ email: filters.email });
    }

    if(filters.name)
    {
        query.where({ name: filters.name });
    }

    return (await query).map(Account.fromDB);
}

export async function getRoles(accountID : number) : Promise<string[]>
{
    const db = await getDB();
    const roles = await db('account as ac')
        .select('r.name as name', 'r.role_id as id')
        .join('account_role as ar', 'ac.account_id', '=', 'ar.account_id')
        .join('role as r', 'ar.role_id', '=', 'r.role_id')
        .where({
            'ac.account_id': accountID,
        });

    return roles.map((role) => role.name);
}

export async function get(accountID : number) : Promise<Account>
{
    const db = await getDB();
    const accounts = await db('account')
        .select(
            'account_id as id',
            'email',
            'name',
            'avatar',
            'permissions',
            'settings'
        )
        .where({
            account_id: accountID,
        });

    if(accounts.length > 1)
    {
        throw new MultipleResultsError('account');
    }
    else if(accounts.length === 0)
    {
        throw new NotFoundError(`No account record found for account '${ accountID }'.`);
    }
    else
    {
        const groups = await getRoles(accountID);
        return Account.fromDB({ ...accounts[0], groups });
    }
}

export async function getByEmail(email : string) : Promise<Account>
{
    const db = await getDB();
    const accounts = await db('account')
        .select(
            'account_id as id',
            'email',
            'name',
            'avatar',
            'permissions',
            'settings'
        )
        .where({ email });

    if(accounts.length > 1)
    {
        throw new MultipleResultsError('account');
    }
    else if(accounts.length === 0)
    {
        throw new NotFoundError(`No account record found with email '${ email }'.`);
    }
    else
    {
        const groups = await getRoles(accounts[0].id);
        return Account.fromDB({ ...accounts[0], groups });
    }
}

export async function add(newAccount : Record<string, unknown>) : Promise<Account>
{
    const account = Account.fromJSON({ ...newAccount, id: undefined, created: Date.now() });
    const db = await getDB();
    const [ accountID ] = await db('account')
        .insert(account.toDB());

    return get(accountID);
}

export async function update(accountID : number, accountUpdate : Record<string, unknown>) : Promise<Account>
{
    // Get the current account
    const account = await get(accountID);

    // Mix the current account with the allowed updates.
    const allowedUpdate = {
        ...account.toJSON(),
        name: accountUpdate.name ?? account.name,
        avatar: accountUpdate.avatar ?? account.avatar,
        settings: accountUpdate.settings ?? account.settings,
    };

    // Make a new account object
    const newAccount = Account.fromJSON(allowedUpdate);

    // Update the database
    const db = await getDB();
    await db('account')
        .update(newAccount.toDB())
        .where({ account_id: accountID });

    // Return the updated record
    return get(accountID);
}

export async function remove(accountID : number) : Promise<void>
{
    const db = await getDB();
    await db('account')
        .where({ account_id: accountID })
        .delete();
}

// ---------------------------------------------------------------------------------------------------------------------
