// ---------------------------------------------------------------------------------------------------------------------
// Environment Resource Access
// ---------------------------------------------------------------------------------------------------------------------

// Managers
import { hasPerm } from './permissions.js';
import authMan from '../auth/authentication.js';

// Models
import { Account } from '../models/account.js';
import { Environment } from '../models/environment.js';

// Interfaces
import { EnvironmentFilters } from '../../common/interfaces/filters.js';

// Utils
import { getDB } from '../utils/database.js';
import { shortID } from '../../common/utils/misc.js';

// Errors
import { AlreadyExistsError, MultipleResultsError, NotAuthorizedError, NotFoundError } from '../errors.js';

// ---------------------------------------------------------------------------------------------------------------------

/* eslint-disable camelcase */

export async function list(filters : EnvironmentFilters, account ?: Account) : Promise<Environment[]>
{
    const db = await getDB();
    const query = db('environment as e')
        .select(
            'e.env_id as id',
            'e.name',
            'e.host',
            'e.port',
            'e.db',
            'e.backend',
            'e.username',
            'e.password',
            'e.secure',
            'e.account_id as accountID'
        );

    if(account)
    {
        query.where(function()
        {
            this.where({ 'e.account_id': null }).orWhere({ 'e.account_id': account.id });
        });
    }
    else
    {
        query.where({ 'e.account_id': null });
    }

    if(filters.email && account)
    {
        query
            .leftJoin('account', 'e.account_id', 'account.account_id')
            .where({ 'account.email': filters.email });
    }

    if(filters.id)
    {
        query.where('e.env_id', 'LIKE', `%${ filters.id }%`);
    }

    return (await query)
        .map((env) =>
        {
            return Environment.fromDB(env);
        });
}

export async function get(id : string, account ?: Account | null) : Promise<Environment>
{
    const db = await getDB();
    const query = db('environment')
        .select(
            'name',
            'env_id as id',
            'host',
            'port',
            'db',
            'backend',
            'username',
            'password',
            'secure',
            'account_id as accountID'
        )
        .where({ env_id: id });

    if(account)
    {
        query.where(function()
        {
            this.where({ account_id: null }).orWhere({ account_id: account.id });
        });
    }
    else if(account === null)
    {
        query.where({ account_id: null });
    }

    // Get environments
    const environments = await query;

    if(environments.length > 1)
    {
        throw new MultipleResultsError('environment');
    }
    else if(environments.length === 0)
    {
        throw new NotFoundError(`No environment record found for environment '${ id }'.`);
    }
    else
    {
        const { environment_id, ...restEnvironment } = environments[0];
        return Environment.fromDB(restEnvironment);
    }
}

export async function add(newEnvironment : Record<string, unknown>, account : Account) : Promise<Environment>
{
    const db = await getDB();
    const environment = Environment.fromJSON({
        ...newEnvironment,
        created: Date.now(),
        updated: Date.now(),
        id: shortID(),
    });

    // Check we have permissions to update system environments
    if(authMan.authEnabled)
    {
        if(environment.accountID === null)
        {
            if(!hasPerm(account, 'canAdd/SystemEnvironments'))
            {
                throw new NotAuthorizedError('add', environment.id as string);
            }
        }
        else if(environment.accountID !== account.id)
        {
            if(!hasPerm(account, 'canAdd/SystemEnvironments'))
            {
                throw new NotAuthorizedError('add', environment.id as string);
            }
        }
    }

    const existing = await get(environment.id, null)
        .catch((error) =>
        {
            if(error.code !== 'ERR_NOT_FOUND')
            {
                throw error;
            }
        });

    if(existing)
    {
        throw new AlreadyExistsError('environment', environment.id);
    }

    await db('environment')
        .insert(environment.toDB())
        .catch((error) =>
        {
            if(error.code === 'SQLITE_CONSTRAINT')
            {
                throw new AlreadyExistsError('environment', environment.id);
            }
            else
            {
                throw error;
            }
        });

    return get(environment.id, account);
}

export async function update(id : string, envUpdate : Record<string, unknown>, account : Account) : Promise<void>
{
    const db = await getDB();

    // Don't allow updating the account id or id.
    delete envUpdate.accountID;
    delete envUpdate.id;

    // Get the current environment
    const environment = await get(id, account);

    // Check we have permissions to update system environments
    if(authMan.authEnabled)
    {
        if(environment.accountID === -1)
        {
            if(!hasPerm(account, 'canUpdate/SystemEnvironments'))
            {
                throw new NotAuthorizedError('update', id);
            }
        }
        else if(environment.accountID !== account.id)
        {
            if(!hasPerm(account, 'canUpdate/SystemEnvironments'))
            {
                throw new NotAuthorizedError('update', id);
            }
        }
    }

    // Mix the current environment with the allowed updates.
    const allowedUpdate = {
        ...environment.toJSON(),
        ...envUpdate,
    };

    // Make a new environment object
    const newEnvironment = Environment.fromJSON(allowedUpdate);

    // Update the database
    await db('environment')
        .update(newEnvironment.toDB())
        .where({ env_id: id });
}

export async function remove(id : string, account : Account) : Promise<{ status : 'ok' }>
{
    const db = await getDB();

    // Get the current environment
    const environment = await get(id);

    // Check we have permissions to update system environments
    if(authMan.authEnabled)
    {
        if(environment.accountID === -1)
        {
            if(!hasPerm(account, 'canDelete/SystemEnvironments'))
            {
                throw new NotAuthorizedError('delete', id);
            }
        }
        else if(environment.accountID !== account.id)
        {
            if(!hasPerm(account, 'canDelete/SystemEnvironments'))
            {
                throw new NotAuthorizedError('delete', id);
            }
        }
    }

    await db('environment')
        .where({ env_id: id })
        .delete();

    return { status: 'ok' };
}

// ---------------------------------------------------------------------------------------------------------------------
