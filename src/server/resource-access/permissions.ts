// ---------------------------------------------------------------------------------------------------------------------
// PermissionsResourceAccess
//----------------------------------------------------------------------------------------------------------------------

import tp from 'trivialperms';

// Managers
import * as rolesRA from './roles.js';

// Models
import { Account } from '../models/account.js';

//----------------------------------------------------------------------------------------------------------------------

export async function init() : Promise<void>
{
    const roles = (await rolesRA.list()) ?? [];
    tp.default.loadGroups(roles);
}

export function hasPerm(user : Account, perm : string) : boolean
{
    return tp.default.hasPerm(user, perm);
}

export function hasGroup(user : Account, groupName : string) : boolean
{
    return tp.default.hasGroup(user, groupName);
}

// ---------------------------------------------------------------------------------------------------------------------
