// ---------------------------------------------------------------------------------------------------------------------
// Roles ResourceAccess
// ---------------------------------------------------------------------------------------------------------------------

// Utils
import { getDB } from '../utils/database.js';

// Models
import { Role } from '../models/role.js';
import { NotFoundError } from '../errors.js';

// ---------------------------------------------------------------------------------------------------------------------

export async function get(roleID : string) : Promise<Role>
{
    const db = await getDB();
    const [ role ] = await db('role as r')
        .select(
            'r.role_id as id',
            'r.name',
            'r.permissions'
        )
        .where({ 'r.role_id': roleID });

    if(!role)
    {
        throw new NotFoundError(`No role record found for account '${ roleID }'.`);
    }

    return Role.fromDB(role);
}

export async function list() : Promise<Role[]>
{
    const db = await getDB();
    return (
        await db('role as r')
            .select(
                'r.role_id as id',
                'r.name',
                'r.permissions'
            )
    )
        .map(Role.fromDB);
}

// ---------------------------------------------------------------------------------------------------------------------
