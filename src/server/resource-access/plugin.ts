/* eslint-disable camelcase */
// ---------------------------------------------------------------------------------------------------------------------
// Plugin Resource Access
// ---------------------------------------------------------------------------------------------------------------------

import { join } from 'node:path';
import { logging } from '@strata-js/strata';
import configUtil from '@strata-js/util-config';
import authMan from '../auth/authentication.js';

// Utils
import { getDB } from '../utils/database.js';
import { hasPerm } from './permissions.js';

// Models
import { PluginStorage } from '../models/pluginStorage.js';

// Interfaces
import { ServerConfig } from '../../common/interfaces/config.js';
import {
    SSTPluginConfig,
    SSTPluginStorage,
    SSTServerPlugin } from '../../common/interfaces/plugins.js';
import { AlreadyExistsError, MultipleResultsError, NotAuthorizedError, NotFoundError } from '../errors.js';
import { Account } from '../models/account.js';

//----------------------------------------------------------------------------------------------------------------------

const logger = logging.getLogger('plugin-manager');

// ---------------------------------------------------------------------------------------------------------------------

/* eslint-disable no-await-in-loop */

async function $safeLoad(path : string) : Promise<SSTPluginConfig | undefined>
{
    try
    {
        return await import(path);
    }
    catch (ex)
    {
        logger.trace(`Failed to import plugin: ${ path }:`, ex);
        return undefined;
    }
}

export async function register() : Promise<SSTPluginConfig[]>
{
    const config = configUtil.get<ServerConfig>();
    const pluginPaths = config?.plugins ?? [];

    const loadedPlugins : SSTPluginConfig[] = [];
    for(const pluginPath of pluginPaths)
    {
        let plugin = await $safeLoad(pluginPath);
        if(!plugin)
        {
            // Attempt to load from the plugins folder
            plugin = await $safeLoad(join(import.meta.dirname, '..', '..', '..', 'plugins', pluginPath, 'index.js'));
        }

        if(!plugin)
        {
            logger.warn(`Failed to import plugin: ${ pluginPath }`);
        }
        else
        {
            loadedPlugins.push(plugin);
            logger.debug('Successfully imported plugin:', pluginPath);
        }
    }

    return loadedPlugins;
}

export async function load(pluginConfig : SSTPluginConfig) : Promise<SSTServerPlugin>
{
    try
    {
        const pluginModule = await import(pluginConfig.server);
        if(pluginModule)
        {
            logger.debug('Successfully loaded plugin:', pluginConfig.server);
            return pluginModule;
        }
    }
    catch (ex)
    {
        logger.warn(`Failed to load plugin: ${ pluginConfig.server }`, ex);
    }

    logger.warn(`Failed to load plugin: ${ pluginConfig.server }`);
}

export async function getStorage(plugin : string, accountId ?: number)
    : Promise<SSTPluginStorage>
{
    const db = await getDB();
    const query = db('plugin_storage')
        .select('plugin', 'storage', 'account_id as accountId')
        .where({ plugin });

    if(accountId)
    {
        query.where({ account_id: accountId });
    }
    else
    {
        query.whereNull('account_id');
    }

    const results = await query;

    if(results.length > 1)
    {
        throw new MultipleResultsError('plugin_storage');
    }
    else if(results.length === 0)
    {
        throw new NotFoundError(`No storage found for plugin '${ plugin }'.`);
    }
    else
    {
        return PluginStorage.fromDB(results[0]);
    }
}

export async function storageExists(plugin : string, accountId ?: number)
    : Promise<boolean>
{
    const db = await getDB();
    const query = db('plugin_storage')
        .count('plugin as count')
        .where({ plugin })
        .first();

    if(accountId)
    {
        query.where({ account_id: accountId });
    }
    else
    {
        query.whereNull('account_id');
    }

    const results = await query;

    return Number.parseInt(results['count'].toString()) > 0;
}

export async function listStorage(accountId ?: number) : Promise<SSTPluginStorage[]>
{
    const db = await getDB();
    const query = db('plugin_storage')
        .select('plugin', 'storage', 'account_id as accountId');

    if(accountId)
    {
        query.where(function()
        {
            this.whereNull('account_id').orWhere({ account_id: accountId });
        });
    }
    else
    {
        query.whereNull('account_id');
    }

    const results = await query;

    return results.map((result) => PluginStorage.fromDB(result));
}

export async function addStorage(newRecord : SSTPluginStorage, account ?: Account)
    : Promise<SSTPluginStorage>
{
    const db = await getDB();

    // Check we have permissions to update system plugin storage
    if(authMan.authEnabled)
    {
        if(newRecord.accountId === null)
        {
            if(!hasPerm(account, 'canAdd/SystemPluginStorage'))
            {
                throw new NotAuthorizedError('add', newRecord.plugin);
            }
        }
        else if(newRecord.accountId !== account?.id)
        {
            if(!hasPerm(account, 'canAdd/SystemPluginStorage'))
            {
                throw new NotAuthorizedError('add', newRecord.plugin);
            }
        }
    }

    const existing = await getStorage(newRecord.plugin, newRecord.accountId)
        .catch((error) =>
        {
            if(error.code !== 'ERR_NOT_FOUND')
            {
                throw error;
            }
        });

    if(existing)
    {
        throw new AlreadyExistsError('plugin_storage', newRecord.plugin);
    }

    await db('plugin_storage')
        .insert(new PluginStorage(newRecord).toDB())
        .catch((error) =>
        {
            if(error.code === 'SQLITE_CONSTRAINT')
            {
                throw new AlreadyExistsError('plugin_storage', newRecord.plugin);
            }
            else
            {
                throw error;
            }
        });

    return getStorage(newRecord.plugin, newRecord.accountId);
}

export async function removeStorage(plugin : string, accountId ?: number, account ?: Account)
    : Promise<{ status : 'ok' }>
{
    const db = await getDB();

    // Get the current environment
    const pluginStorage = await getStorage(plugin, accountId);

    // Check we have permissions to update system plugin storage
    if(authMan.authEnabled)
    {
        if(pluginStorage.accountId === -1 || pluginStorage.accountId === null)
        {
            if(!hasPerm(account, 'canDelete/SystemPluginStorage'))
            {
                throw new NotAuthorizedError('delete', plugin);
            }
        }
        else if(pluginStorage.accountId !== account.id)
        {
            if(!hasPerm(account, 'canDelete/SystemPluginStorage'))
            {
                throw new NotAuthorizedError('delete', plugin);
            }
        }
    }

    await db('plugin_storage')
        .where({ plugin: pluginStorage.plugin, account_id: pluginStorage.accountId })
        .delete();

    return { status: 'ok' };
}

export async function updateStorage(
    record : SSTPluginStorage,
    account ?: Account
)
    : Promise<void>
{
    const db = await getDB();

    // Get the current plugin storage
    const pluginStorage = await getStorage(record.plugin, record.accountId);

    // Check we have permissions to update system plugin storage
    if(authMan.authEnabled)
    {
        if(pluginStorage.accountId === -1 || pluginStorage.accountId === null)
        {
            if(!hasPerm(account, 'canUpdate/SystemPluginStorage'))
            {
                throw new NotAuthorizedError('update', record.plugin);
            }
        }
        else if(pluginStorage.accountId !== account.id)
        {
            if(!hasPerm(account, 'canUpdate/SystemPluginStorage'))
            {
                throw new NotAuthorizedError('update', record.plugin);
            }
        }
    }

    pluginStorage.storage = record.storage;

    // Update the database
    await db('plugin_storage')
        .update(new PluginStorage(pluginStorage).toDB())
        .where({ plugin: record.plugin, account_id: record.accountId });
}

// ---------------------------------------------------------------------------------------------------------------------
