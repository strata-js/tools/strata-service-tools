//----------------------------------------------------------------------------------------------------------------------
// Routes for Environments
//----------------------------------------------------------------------------------------------------------------------

import express from 'express';

import { ensureAuthenticated, errorHandler, wrapAsync } from './utils/index.js';

// Managers
import * as environmentMan from '../managers/environment.js';

//----------------------------------------------------------------------------------------------------------------------

const router = express.Router();

//----------------------------------------------------------------------------------------------------------------------

router.get('/', wrapAsync(async(req, resp) =>
{
    const filters = { id: req.query.id, email: req.query.email };
    resp.json((await environmentMan.list(filters, req.user)));
}));

router.post('/', ensureAuthenticated, wrapAsync(async(req, resp) =>
{
    // Add the environment
    const newEnvironment = await environmentMan.add(req.body, req.user);
    resp.json(newEnvironment);
}));

router.get('/:id', wrapAsync(async(req, resp) =>
{
    const environment = await environmentMan.get(req.params.id, req.user);

    if(req.isAuthenticated())
    {
        resp.json(environment);
    }
    else
    {
        resp.json(environment);
    }
}));

router.patch('/:id', ensureAuthenticated, wrapAsync(async(req, resp) =>
{
    // Update the environment
    const newEnvironment = await environmentMan.update(req.params.id, req.body, req.user);
    resp.json(newEnvironment);
}));

router.delete('/:id', ensureAuthenticated, wrapAsync(async(req, resp) =>
{
    // Delete the environment
    await environmentMan.remove(req.params.id, req.user);
    resp.end();
}));

//----------------------------------------------------------------------------------------------------------------------
// Error Handling
//----------------------------------------------------------------------------------------------------------------------

router.use(errorHandler());

//----------------------------------------------------------------------------------------------------------------------

export default router;

//----------------------------------------------------------------------------------------------------------------------
