//----------------------------------------------------------------------------------------------------------------------
// Routes for Configuration
//----------------------------------------------------------------------------------------------------------------------

import { resolve } from 'node:path';
import fs from 'node:fs';
import express from 'express';
import mime from 'mime-types';
import { logging } from '@strata-js/strata';

// Managers
import { clientPlugins } from '../managers/plugin.js';

// Resource Access
import {
    addStorage,
    getStorage,
    listStorage,
    removeStorage,
    storageExists,
    updateStorage,
} from '../resource-access/plugin.js';

// Utils
import { ensureAuthenticated, errorHandler, wrapAsync } from './utils/index.js';

//----------------------------------------------------------------------------------------------------------------------

const router = express.Router();
const logger = logging.getLogger('plugin-router');

//----------------------------------------------------------------------------------------------------------------------

router.get('/', async(_req, resp) =>
{
    resp.json(clientPlugins.map((plugin) => plugin.name));
});

router.get('/load/:name', async(req, resp) =>
{
    const pluginName = req.params.name;
    const plugin = clientPlugins.find((pluginCfg) => pluginCfg.name === pluginName);

    if(plugin)
    {
        resp.setHeader('Content-Type', mime.lookup(plugin.client));
        fs.createReadStream(resolve(plugin.client))
            .on('error', (err) =>
            {
                resp.setHeader('Content-Type', 'application/json');
                resp.status(500).json({ error: `Failed to serve plugin: '${ pluginName }'.` });
                logger.warn(`Failed to serve plugin: '${ pluginName }':`, err);
            })
            .pipe(resp);
    }
    else
    {
        resp.status(404).json({ error: `Plugin "${ pluginName }" not found.` });
    }
});

router.get('/storage', wrapAsync(async(req, resp) =>
{
    // List plugin storage
    resp.json(await listStorage(req.user?.id));
}));

router.get('/storage/:name', wrapAsync(async(req, resp) =>
{
    // Get plugin storage
    const pluginName = req.params.name;
    const accountId : number = req.query.accountId;
    resp.json(await getStorage(pluginName, accountId));
}));

router.get('/storage/exists/:name', wrapAsync(async(req, resp) =>
{
    // Get plugin storage
    const pluginName = req.params.name;
    const accountId : number = req.query.accountId;
    resp.json(await storageExists(pluginName, accountId));
}));

router.post('/storage', ensureAuthenticated, wrapAsync(async(req, resp) =>
{
    // Add plugin storage
    resp.json(await addStorage(req.body, req.user));
}));

router.patch('/storage', ensureAuthenticated, wrapAsync(async(req, resp) =>
{
    // Update plugin storage
    resp.json(updateStorage(req.body, req.user));
}));

router.delete('/storage/:name', ensureAuthenticated, wrapAsync(async(req, resp) =>
{
    // Delete the plugin storage
    const pluginName : string = req.params.name;
    const accountId : number = req.query.accountId;
    await removeStorage(pluginName, accountId, req.user);
    resp.end();
}));

//----------------------------------------------------------------------------------------------------------------------
// Error Handling
//----------------------------------------------------------------------------------------------------------------------

router.use(errorHandler());

//----------------------------------------------------------------------------------------------------------------------

export default router;

//----------------------------------------------------------------------------------------------------------------------
