//----------------------------------------------------------------------------------------------------------------------
// Add Redis DB Support
//----------------------------------------------------------------------------------------------------------------------

import { Knex } from 'knex';

//----------------------------------------------------------------------------------------------------------------------

export async function up(knex : Knex) : Promise<void>
{
    await knex.schema.alterTable('environment', (table) =>
    {
        // Add db column to the environment table.
        table.integer('db')
            .notNullable()
            .defaultTo(0);
    });
}

//----------------------------------------------------------------------------------------------------------------------

export async function down(knex : Knex) : Promise<void>
{
    // Drop db column from the environment table.
    await knex.schema.alterTable('environment', (table) =>
    {
        table.dropColumn('db');
    });
}

//----------------------------------------------------------------------------------------------------------------------
