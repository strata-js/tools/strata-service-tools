import type { Knex } from 'knex';

export async function up(knex : Knex) : Promise<void>
{
    // The `plugin_storage` table
    await knex.schema.createTable('plugin_storage', (table) =>
    {
        table.string('plugin');
        table.json('storage').nullable();
        table.integer('account_id')
            .references('account.account_id')
            .onUpdate('CASCADE')
            .onDelete('CASCADE')
            .nullable();
        table.unique([ 'plugin', 'account_id' ]);
    });
}

export async function down(knex : Knex) : Promise<void>
{
    await knex.schema.dropTable('plugin_storage');
}

