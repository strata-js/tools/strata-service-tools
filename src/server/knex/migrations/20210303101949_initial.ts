//----------------------------------------------------------------------------------------------------------------------
// Initial DB Migration
//----------------------------------------------------------------------------------------------------------------------

import { Knex } from 'knex';

//----------------------------------------------------------------------------------------------------------------------

export async function up(knex : Knex) : Promise<void>
{
    // The `account` table
    await knex.schema.createTable('account', (table) =>
    {
        table.integer('account_id').primary();
        table.text('email').notNullable()
            .unique()
            .index();
        table.text('name');
        table.text('avatar');
        table.json('permissions').notNullable()
            .defaultTo('[]');
        table.json('settings').notNullable()
            .defaultTo('{}');
        table.timestamp('created').notNullable()
            .defaultTo(knex.fn.now());
    });

    // The `role` table
    await knex.schema.createTable('role', (table) =>
    {
        table.integer('role_id').primary();
        table.text('name').notNullable()
            .unique();
        table.json('permissions').notNullable()
            .defaultTo('[]');
    });

    // The `account_role` table
    await knex.schema.createTable('account_role', (table) =>
    {
        table.integer('account_id')
            .references('account.account_id')
            .notNullable()
            .onUpdate('CASCADE')
            .onDelete('CASCADE');
        table.integer('role_id')
            .references('role.role_id')
            .notNullable()
            .onUpdate('CASCADE')
            .onDelete('CASCADE');
        table.unique([ 'account_id', 'role_id' ]);
    });
}

//----------------------------------------------------------------------------------------------------------------------

export async function down(knex : Knex) : Promise<void>
{
    await knex.schema.dropTable('account');
    await knex.schema.dropTable('role');
    await knex.schema.dropTable('account_role');
}

//----------------------------------------------------------------------------------------------------------------------

