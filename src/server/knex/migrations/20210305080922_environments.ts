//----------------------------------------------------------------------------------------------------------------------
// Environments
//----------------------------------------------------------------------------------------------------------------------

import { Knex } from 'knex';

//----------------------------------------------------------------------------------------------------------------------

export async function up(knex : Knex) : Promise<void>
{
    // The `environment` table
    await knex.schema.createTable('environment', (table) =>
    {
        table.integer('env_id').primary();
        table.string('name')
            .notNullable();
        table.string('slug')
            .notNullable()
            .index();
        table.string('host')
            .notNullable();
        table.integer('port')
            .notNullable();
        table.string('username');
        table.string('password');
        table.boolean('secure')
            .notNullable()
            .defaultTo(false);
        table.integer('account_id')
            .references('account.account_id')
            .onUpdate('CASCADE')
            .onDelete('CASCADE');
        table.timestamp('created').notNullable()
            .defaultTo(knex.fn.now());
        table.timestamp('updated').notNullable()
            .defaultTo(knex.fn.now());

        // Add a generated column to work around limitations with unique constraint and nulls
        table.specificType('gen_account_id', 'INTEGER GENERATED ALWAYS AS (COALESCE(account_id, -1)) STORED');

        // Add a unique constraint
        table.unique([ 'slug', 'gen_account_id' ]);
    });
}

//----------------------------------------------------------------------------------------------------------------------

export async function down(knex : Knex) : Promise<void>
{
    await knex.schema.dropTable('environments');
}

//----------------------------------------------------------------------------------------------------------------------
