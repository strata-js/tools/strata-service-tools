//----------------------------------------------------------------------------------------------------------------------
// Add Backend Support
//----------------------------------------------------------------------------------------------------------------------

import { Knex } from 'knex';

//----------------------------------------------------------------------------------------------------------------------

/* eslint-disable camelcase */

export async function up(knex : Knex) : Promise<void>
{
    // Create a new table, that will hold the values from the old table, but with modifications
    await knex.schema.createTable('tmp_environment', (table) =>
    {
        table.string('env_id').primary();
        table.string('name')
            .notNullable();
        table.string('host')
            .notNullable();
        table.integer('port')
            .notNullable();
        table.string('backend')
            .notNullable()
            .defaultTo('null');
        table.string('username');
        table.string('password');
        table.boolean('secure')
            .notNullable()
            .defaultTo(false);
        table.integer('account_id')
            .references('account.account_id')
            .onUpdate('CASCADE')
            .onDelete('CASCADE');
        table.timestamp('created').notNullable()
            .defaultTo(knex.fn.now());
        table.timestamp('updated').notNullable()
            .defaultTo(knex.fn.now());

        // Add a generated column to work around limitations with unique constraint and nulls
        table.specificType('gen_account_id', 'INTEGER GENERATED ALWAYS AS (COALESCE(account_id, -1)) STORED');
    });

    // Copy the data from the old table to the new table
    let rows = await knex('environment').select('*');

    // Modify the rows to add backend
    rows = rows.map((row) =>
    {
        const { gen_account_id, ...restRow } = row;

        return {
            ...restRow,
            backend: 'redis-streams',
        };
    });

    // Insert the rows into the new table
    await knex.batchInsert('tmp_environment', rows, 100);

    // Drop the old table
    await knex.schema.dropTable('environment');

    // Rename the new table to the old table's name
    await knex.schema.renameTable('tmp_environment', 'environment');

    // Rename unique index
    await knex.schema.alterTable('environment', (table) =>
    {
        table.unique([ 'env_id', 'gen_account_id' ]);
    });
}

//----------------------------------------------------------------------------------------------------------------------

export async function down(knex : Knex) : Promise<void>
{
    // Create a new table, that will hold the values from the old table, but with modifications
    await knex.schema.createTable('tmp_environment', (table) =>
    {
        table.string('env_id').primary();
        table.string('name')
            .notNullable();
        table.string('host')
            .notNullable();
        table.integer('port')
            .notNullable();
        table.string('username');
        table.string('password');
        table.boolean('secure')
            .notNullable()
            .defaultTo(false);
        table.integer('account_id')
            .references('account.account_id')
            .onUpdate('CASCADE')
            .onDelete('CASCADE');
        table.timestamp('created').notNullable()
            .defaultTo(knex.fn.now());
        table.timestamp('updated').notNullable()
            .defaultTo(knex.fn.now());

        // Add a generated column to work around limitations with unique constraint and nulls
        table.specificType('gen_account_id', 'INTEGER GENERATED ALWAYS AS (COALESCE(account_id, -1)) STORED');
    });

    // Copy the data from the old table to the new table
    let rows = await knex('environment').select('*');

    // Modify the rows to update env_id to be the new shortID format
    rows = rows.map((row) =>
    {
        const { gen_account_id, backend, ...restRow } = row;

        return {
            ...restRow,
        };
    });

    // Insert the rows into the new table
    await knex.batchInsert('tmp_environment', rows, 100);

    // Drop the old table
    await knex.schema.dropTable('environment');

    // Rename the new table to the old table's name
    await knex.schema.renameTable('tmp_environment', 'environment');

    // Rename unique index
    await knex.schema.alterTable('environment', (table) =>
    {
        table.unique([ 'env_id', 'gen_account_id' ]);
    });
}

//----------------------------------------------------------------------------------------------------------------------
