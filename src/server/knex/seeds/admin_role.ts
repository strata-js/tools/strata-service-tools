//----------------------------------------------------------------------------------------------------------------------
// Set us up a default role
//----------------------------------------------------------------------------------------------------------------------

/* eslint-disable camelcase */

export async function seed(knex) : Promise<void>
{
    // Delete automatic roles
    await knex('role').del()
        .whereIn('role_id', [ 1, 2 ]);

    // Add Automatic Roles
    await knex('role')
        .insert([
            { role_id: 1, name: 'Admins', permissions: JSON.stringify([ '*/*' ]) },
        ]);
};

//----------------------------------------------------------------------------------------------------------------------
