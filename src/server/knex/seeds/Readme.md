# Knex Seeds

These are the knex seed files. Seeds are a way of populating the database when it's first created. Seeds can also be  
written so that they update that initial data, in case its values need to change. In order to read more on creating  
these seeds using the `knex` command line utility, please see the [documentation][knex].

[knex]: https://knexjs.org/#Seeds
