//----------------------------------------------------------------------------------------------------------------------
// Set us up a default role
//----------------------------------------------------------------------------------------------------------------------

import { shortID } from '../../../common/utils/misc.js';

//----------------------------------------------------------------------------------------------------------------------

/* eslint-disable camelcase */

export async function seed(knex) : Promise<void>
{
    const [ results ] = await (knex('environment')
        .count('env_id', { as: 'count' })
        .catch(() => [ 0 ]));

    if(results.count === 0)
    {
        // Add the local environment
        await knex('environment')
            .insert([
                { env_id: shortID(), name: 'Local', host: 'localhost', port: 6379, db: 0 },
            ]);
    }
};

//----------------------------------------------------------------------------------------------------------------------
