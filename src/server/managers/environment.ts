// ---------------------------------------------------------------------------------------------------------------------
// Environment Manager
// ---------------------------------------------------------------------------------------------------------------------

// Models
import { Account } from '../models/account.js';
import { Environment } from '../models/environment.js';
import { Socket } from 'socket.io';

// Interfaces
import { EnvironmentFilters } from '../../common/interfaces/filters.js';

// Resource Access
import * as envRA from '../resource-access/environment.js';
import { StrataClient, StrataClientConfig } from '@strata-js/strata';

// Utils
import { promiseTimeout } from '../../common/utils/misc.js';
import { emitToAll } from '../utils/sio.js';

// ---------------------------------------------------------------------------------------------------------------------

type ConnectionStatus = 'connecting' | 'connected' | 'disconnected';

const connections = new Map<string, StrataClient>();
const connectionStatus = new Map<string, { status : ConnectionStatus, error ?: string }>();

// ---------------------------------------------------------------------------------------------------------------------

export function sendConnectionStatusUpdate(socket ?: Socket) : void
{
    const statuses = Array.from(connectionStatus).map(([ key, value ]) =>
    {
        return {
            envId: key,
            status: value.status,
            error: value.error,
        };
    });

    if(socket)
    {
        socket.emit('envConnectionStatus', statuses);
    }
    else
    {
        emitToAll('envConnectionStatus', statuses);
    }
}

function handleConnectionStatusChanged(envID : string, status : ConnectionStatus, error ?: string) : void
{
    connectionStatus.set(envID, { status, error });
    sendConnectionStatusUpdate();
}

async function _buildClient(envID : string, account ?: Account) : Promise<StrataClient>
{
    handleConnectionStatusChanged(envID, 'connecting');

    const env = await envRA.get(envID, account);

    const clientConfig : StrataClientConfig = {
        client: {
            name: `SST:${ env.name }`,
        },
        backend: {
            type: env.backend,
            redis: {
                host: env.host,
                port: env.port,
                db: env.db ?? 0,
                username: env.username,
                password: env.password,
            },
        },
    };

    const client = new StrataClient(clientConfig);
    try
    {
        const timeoutMessage = `${ env.name } (${ env.host }:${ env.port }) failed to respond`;
        await promiseTimeout(client.start(), 15000, timeoutMessage);

        // Add it to our connections map
        connections.set(envID, client);

        handleConnectionStatusChanged(envID, 'connected');
    }
    catch (error)
    {
        // If the promise timed out we teardown the client.
        if(error.name === 'PromiseTimeoutError')
        {
            await client.teardown();
        }

        handleConnectionStatusChanged(envID, 'disconnected', error?.message);

        throw error;
    }

    return client;
}

async function _teardownClient(envID : string) : Promise<void>
{
    const client = connections.get(envID);

    if(client)
    {
        await client.teardown();
    }

    connections.delete(envID);

    handleConnectionStatusChanged(envID, 'disconnected');
}

// ---------------------------------------------------------------------------------------------------------------------

export async function list(filters : EnvironmentFilters, account ?: Account) : Promise<Environment[]>
{
    return envRA.list(filters, account);
}

export async function get(envID : string, account ?: Account | null) : Promise<Environment>
{
    return envRA.get(envID, account);
}

export async function getConnection(envID : string, account ?: Account) : Promise<StrataClient>
{
    let client = connections.get(envID);
    if(!client)
    {
        client = await _buildClient(envID, account);
    }

    return client;
}

export async function add(newEnvironment : Record<string, unknown>, account : Account) : Promise<Environment>
{
    return envRA.add(newEnvironment, account);
}

export async function update(envID : string, envObj : Record<string, unknown>, account : Account) : Promise<Environment>
{
    await envRA.update(envID, envObj, account);

    await _teardownClient(envID);
    await _buildClient(envID, account);

    // Return the updated Environment
    return envRA.get(envID, account);
}

export async function remove(envID : string, account : Account) : Promise<{ status : 'ok' }>
{
    await _teardownClient(envID);
    return envRA.remove(envID, account);
}

// ---------------------------------------------------------------------------------------------------------------------
