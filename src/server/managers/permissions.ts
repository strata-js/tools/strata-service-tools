// ---------------------------------------------------------------------------------------------------------------------
// PermissionsManager
//----------------------------------------------------------------------------------------------------------------------

// Models
import { Account } from '../models/account.js';

// Resource Access
import * as permsRA from '../resource-access/permissions.js';

//----------------------------------------------------------------------------------------------------------------------

export async function init() : Promise<void>
{
    await permsRA.init();
}

export function hasPerm(user : Account, perm : string) : boolean
{
    return permsRA.hasPerm(user, perm);
}

export function hasGroup(user : Account, groupName : string) : boolean
{
    return permsRA.hasGroup(user, groupName);
}

// ---------------------------------------------------------------------------------------------------------------------
