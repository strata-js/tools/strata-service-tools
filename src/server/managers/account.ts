// ---------------------------------------------------------------------------------------------------------------------
// Account Manager
// ---------------------------------------------------------------------------------------------------------------------

// Models
import { Account } from '../models/account.js';

// Interfaces
import { AccountFilters } from '../../common/interfaces/filters.js';

// Resource Access
import * as accountRA from '../resource-access/account.js';

// ---------------------------------------------------------------------------------------------------------------------

export async function list(filters : AccountFilters) : Promise<Account[]>
{
    return accountRA.list(filters);
}

export async function getRoles(accountID : number) : Promise<string[]>
{
    return accountRA.getRoles(accountID);
}

export async function get(accountID : number) : Promise<Account>
{
    return accountRA.get(accountID);
}

export async function getByEmail(email : string) : Promise<Account>
{
    return accountRA.getByEmail(email);
}

export async function add(newAccount : Record<string, unknown>) : Promise<Account>
{
    return accountRA.add(newAccount);
}

export async function update(accountID : number, accountUpdate : Record<string, unknown>) : Promise<Account>
{
    return accountRA.update(accountID, accountUpdate);
}

export async function remove(accountID : number) : Promise<void>
{
    return accountRA.remove(accountID);
}

// ---------------------------------------------------------------------------------------------------------------------
