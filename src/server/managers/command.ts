//----------------------------------------------------------------------------------------------------------------------
// Command Manager
//----------------------------------------------------------------------------------------------------------------------

import JSON5 from 'json5';
import { Socket } from 'socket.io';
import { logging } from '@strata-js/strata';

// Interfaces
import { SSTClientRequest } from '../../common/interfaces/request.js';

// Models
import { Account } from '../models/account.js';

// Managers
import { getConnection } from './environment.js';

// ---------------------------------------------------------------------------------------------------------------------

const logger = logging.getLogger('commandMan');

function $registerHandler(socket : Socket, event, handler) : void
{
    socket.on(event, async(payload, callback) => callback(await handler(payload, (socket.request as any).user)));
}

//----------------------------------------------------------------------------------------------------------------------
// Command Handlers
//----------------------------------------------------------------------------------------------------------------------

async function onRequest(request : SSTClientRequest, account ?: Account) : Promise<Record<string, unknown>>
{
    try
    {
        const { context, operation, payload, serviceGroup, auth, env } = request;
        const payloadObj = JSON5.parse(payload);

        const client = await getConnection(env, account);

        return await client.request(
            serviceGroup,
            context,
            operation,
            payloadObj,
            { isSST: true },
            auth
        ) as unknown as Record<string, unknown>;
    }
    catch (ex)
    {
        let error = ex.toJSON?.() ?? ex;
        if(error.response)
        {
            return error.response;
        }
        else
        {
            if(error.innerError)
            {
                error = error.innerError;
            }

            return {
                payload: {
                    name: error.name,
                    message: error.message,
                    code: error.code,
                },
                error,
            };
        }
    }
}

async function onListServices(request : { env : string }, account ?: Account) : Promise<Record<string, unknown>>
{
    const client = await getConnection(request.env, account).catch((error) =>
    {
        logger.warn('Failed to get client:', error.message);
    });

    if(client)
    {
        return client.discoverServices();
    }

    return {};
}

//----------------------------------------------------------------------------------------------------------------------
// Public
//----------------------------------------------------------------------------------------------------------------------

export async function init() : Promise<void>
{
    // TODO: Maybe something goes here?
}

export async function register(socket : Socket) : Promise<void>
{
    // Register Handlers
    $registerHandler(socket, 'request', onRequest);
    $registerHandler(socket, 'listServices', onListServices);
}

//----------------------------------------------------------------------------------------------------------------------
