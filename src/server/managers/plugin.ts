//----------------------------------------------------------------------------------------------------------------------
// Plugin Manager
//----------------------------------------------------------------------------------------------------------------------

import { logging } from '@strata-js/strata';
import { Application } from 'express';

// Interfaces
import { SSTPluginConfig } from '../../common/interfaces/plugins.js';

// Resource Access
import { load, register } from '../resource-access/plugin.js';

//----------------------------------------------------------------------------------------------------------------------

const logger = logging.getLogger('plugin-manager');

const _registeredPlugins = new Map<string, SSTPluginConfig>();
const _clientPlugins : SSTPluginConfig[] = [];
const _serverPlugins : SSTPluginConfig[] = [];

//----------------------------------------------------------------------------------------------------------------------

/* eslint-disable no-await-in-loop */

export async function init(app : Application) : Promise<void>
{
    const plugins = await register();
    for(const plugin of plugins)
    {
        logger.info(`Registering plugin: ${ plugin.name }...`);

        _registeredPlugins.set(plugin.name, plugin);

        if(plugin.client)
        {
            _clientPlugins.push(plugin);
        }

        if(plugin.server)
        {
            _serverPlugins.push(plugin);
        }
    }

    logger.info('All plugins registered.');
    logger.info('Loading registered sever plugins...');

    // Get a list of only plugins that have a server configured.
    for(const pluginCfg of _serverPlugins)
    {
        logger.info(`Loading plugin: ${ pluginCfg.name }...`);
        const plugin = await load(pluginCfg);
        if(plugin.registerServer)
        {
            try
            {
                await plugin.registerServer(app);
            }
            catch (ex)
            {
                logger.error(`Failed to register plugin: ${ pluginCfg.name }:`, ex);
            }

            logger.info(`Successfully registered plugin: ${ pluginCfg.name }.`);
        }
        else
        {
            logger.warn(`Plugin: ${ pluginCfg.name } does not have a 'registerServer' method.`);
        }
    }
}

//----------------------------------------------------------------------------------------------------------------------

export const clientPlugins = _clientPlugins;

//----------------------------------------------------------------------------------------------------------------------
