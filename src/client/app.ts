//----------------------------------------------------------------------------------------------------------------------
// Strata Service Tools Client Application
//----------------------------------------------------------------------------------------------------------------------

import * as Vue from 'vue';
import { createRouter, createWebHistory } from 'vue-router';

// Bootstrap Vue
import { createBootstrap } from 'bootstrap-vue-next';

// Font Awesome
import { library } from '@fortawesome/fontawesome-svg-core';
import { fab } from '@fortawesome/free-brands-svg-icons';
import { far } from '@fortawesome/pro-regular-svg-icons';
import { fas } from '@fortawesome/pro-solid-svg-icons';
import { FontAwesomeIcon, FontAwesomeLayers } from '@fortawesome/vue-fontawesome';

// Lib
import { ensureAuthenticationState } from './lib/navigation.js';

// Views
import AppComponent from './app.vue';

// Pages
import HomePage from './pages/homePage.vue';
import RequestsPage from './pages/requestsPage.vue';

// Managers
import authMan from './lib/managers/auth.js';
import requestMan from './lib/managers/request.js';
import envMan from './lib/managers/environments.js';
import * as permsMan from './lib/managers/permissions.js';
import pluginMan from './lib/managers/plugin.js';
import versionMan from './lib/managers/version.js';

// Resource Access
import colorMode from './lib/resource-access/colorMode.js';
import sioRA from './lib/resource-access/sio.js';

// Stylesheets
import './scss/theme.scss';

// ---------------------------------------------------------------------------------------------------------------------
// Font Awesome
// ---------------------------------------------------------------------------------------------------------------------

library.add(fab, far, fas);

//----------------------------------------------------------------------------------------------------------------------
// Vue Router
//----------------------------------------------------------------------------------------------------------------------

const router = createRouter({
    history: createWebHistory(),
    routes: [
        {
            path: '/',
            name: 'home',
            component: HomePage,
            beforeEnter: ensureAuthenticationState('logged out'),
        },
        {
            path: '/requests/',
            name: 'requests',
            component: RequestsPage,
            beforeEnter: ensureAuthenticationState('logged in'),
        },
    ],
});

//----------------------------------------------------------------------------------------------------------------------
// Setup Vue App
//----------------------------------------------------------------------------------------------------------------------

// Set up app component
const app = Vue.createApp(AppComponent)
    .component('Fa', FontAwesomeIcon)
    .component('FaLayers', FontAwesomeLayers)
    .use(createBootstrap({ modalController: true }))
    .use(router);

// window.Vue is set here to use in client plugins
(window as any).Vue = Vue;

//----------------------------------------------------------------------------------------------------------------------
// App Initialization
//----------------------------------------------------------------------------------------------------------------------

async function init() : Promise<void>
{
    // Resource Access
    colorMode.setColorMode('auto');
    await sioRA.init();

    // Managers
    await versionMan.init();
    await requestMan.init();
    await permsMan.init();
    await authMan.init();
    await envMan.init();
    await pluginMan.init(app);

    // Mount the application
    app.mount('#app');
}

//----------------------------------------------------------------------------------------------------------------------

init()
    .then(() => console.info(`Strata Service Tools v${ versionMan.version } started.`));

//----------------------------------------------------------------------------------------------------------------------
