<!----------------------------------------------------------------------------------------------------------------------
  -- Request Form
  --------------------------------------------------------------------------------------------------------------------->

<template>
    <div id="request-form">
        <BFormRow>
            <BCol>
                <BFormGroup
                    id="service-list-group"
                    description="Available service groups"
                    label="Service Groups"
                    label-for="service-list"
                >
                    <div class="d-flex">
                        <RequestSelect
                            v-model="request.serviceGroup"
                            :options="serviceGroups"
                            placeholder="ex: MyService"
                            :disabled="servicesLoading"
                            input-id="service-list"
                            @clear="onClearService()"
                        />
                        <BButton class="ms-2 pt-0 pb-0" :disabled="servicesLoading" @click="refreshServiceList()">
                            <Fa icon="sync-alt" :spin="servicesLoading" />
                        </BButton>
                    </div>
                </BFormGroup>
            </BCol>
            <BCol>
                <Component
                    :is="authInput"
                    v-model:auth="request.auth"
                    :account="account"
                    :disabled="isDisabled"
                />
            </BCol>
        </BFormRow>
        <BFormRow>
            <BCol>
                <BFormGroup
                    id="context-group"
                    description="Request context"
                    label="Context"
                    label-for="context-list"
                >
                    <RequestSelect
                        v-model="request.context"
                        :options="contexts"
                        placeholder="ex: insured, adm, etc."
                        :disabled="isDisabled"
                        input-id="context-list"
                        @clear="onClearContext()"
                    />
                </BFormGroup>
            </BCol>
            <BCol>
                <BFormGroup
                    id="operation-group"
                    description="Request operation"
                    label="Operation"
                    label-for="operation-list"
                >
                    <RequestSelect
                        v-model="request.operation"
                        :options="operations"
                        placeholder="ex: list, load, etc."
                        :disabled="isDisabled"
                        input-id="operation-list"
                    />
                </BFormGroup>
            </BCol>
        </BFormRow>
        <BCard class="overflow-hidden" no-body>
            <template #header>
                <div class="float-end">
                    <BButton size="sm" @click="reformatPayload()">
                        <Fa icon="code" />
                        Reformat
                    </BButton>
                </div>
                <h6 class="mt-1 mb-0">
                    Payload
                </h6>
            </template>
            <Codemirror ref="editor" v-model="request.payload" :extensions="cmExtensions" :tab-size="4" />
        </BCard>

        <!-- Buttons -->
        <BFormRow class="mt-2">
            <BCol class="pe-1">
                <BButton
                    :variant="isSecure ? 'warning' : 'primary'"
                    class="w-100"
                    :disabled="!isRequestValid() || isDisabled || responseOutstanding"
                    @click="sendRequest()"
                >
                    <BSpinner v-if="responseOutstanding" small />
                    <span v-else>
                        <Fa v-if="isSecure" icon="lock" />
                        Send Request
                    </span>
                </BButton>
            </BCol>
            <BCol class="ps-1">
                <BButton
                    v-if="responseOutstanding"
                    class="w-100"
                    variant="danger"
                    @click="cancelRequest()"
                >
                    Cancel Request
                </BButton>
                <BButton
                    v-else
                    class="w-100"
                    :disabled="isDisabled"
                    @click="resetForm()"
                >
                    Reset Form
                </BButton>
            </BCol>
        </BFormRow>
    </div>
</template>

<!--------------------------------------------------------------------------------------------------------------------->

<style lang="scss">
    @media screen and (min-width: 1172px) and (min-height: 470px) {
        #request-form {
            .cm-editor {
                height: calc(100vh - 383px) !important;
            }
        }
    }
</style>

<!--------------------------------------------------------------------------------------------------------------------->

<script lang="ts" setup>
    import { computed, onBeforeUnmount, onMounted, ref, watch } from 'vue';
    import { useObservable } from '../../lib/composables/rxjs.js';

    import { linter } from '@codemirror/lint';
    import { json5, json5ParseLinter } from 'codemirror-json5';
    import { oneDark } from '@codemirror/theme-one-dark';

    // Components
    import { Codemirror } from 'vue-codemirror';
    import RequestSelect from './requestSelect.vue';
    import SimpleAuth from './simpleAuth.vue';

    // Managers
    import authMan from '../../lib/managers/auth';
    import envMan from '../../lib/managers/environments.js';
    import requestMan from '../../lib/managers/request.js';
    import pluginMan from '../../lib/managers/plugin.js';

    // Resource Access
    import colorMode from '../../lib/resource-access/colorMode';
    import { SSTClientRequest } from '../../../common/interfaces/request.js';

    //------------------------------------------------------------------------------------------------------------------
    // Refs
    //------------------------------------------------------------------------------------------------------------------

    const baseCMExtensions = [ json5(), linter(json5ParseLinter()) ];
    const bsTheme = useObservable(colorMode.bsTheme$);
    const refreshInterval = 300 * 1000;
    const refreshTimer = ref<ReturnType<typeof setInterval> | undefined>(undefined);
    const selectedServiceGroup = ref<string | undefined>(undefined);

    //------------------------------------------------------------------------------------------------------------------
    // Observables
    //------------------------------------------------------------------------------------------------------------------

    const account = useObservable(authMan.account$);
    const currentEnv = useObservable(envMan.currentEnv$);
    const pluginConfigs = useObservable(pluginMan.configs$);
    const request = useObservable(requestMan.request$);
    const responseOutstanding = useObservable(requestMan.responseOutstanding$);
    const serviceList = useObservable(requestMan.services$);
    const servicesLoading = useObservable(requestMan.servicesLoading$);

    //------------------------------------------------------------------------------------------------------------------
    // Computed
    //------------------------------------------------------------------------------------------------------------------

    const authInput = computed(() =>
    {
        const authConfig = pluginConfigs.value.find((config) => config.auth);
        return authConfig?.auth ?? SimpleAuth;
    });

    const cmExtensions = computed(() =>
    {
        if(bsTheme.value === 'dark')
        {
            return [
                ...baseCMExtensions,
                oneDark,
            ];
        }

        return baseCMExtensions;
    });

    const isDisabled = computed(() => !currentEnv.value);
    const isSecure = computed(() => !!currentEnv.value?.secure);
    const serviceGroups = computed(() =>
    {
        if(serviceList.value)
        {
            return Object.keys(serviceList.value).sort();
        }

        return [];
    });

    const contexts = computed(() =>
    {
        const serviceInstances = serviceList.value?.[request.value.serviceGroup];
        if(serviceInstances)
        {
            const firstInstance = Object.keys(serviceInstances)[0];
            return Object.keys(serviceInstances[firstInstance].contexts).sort();
        }

        return [];
    });

    const operations = computed(() =>
    {
        const serviceInstances = serviceList.value?.[request.value.serviceGroup];
        if(serviceInstances)
        {
            const firstInstance = Object.keys(serviceInstances)[0];
            return (serviceInstances[firstInstance].contexts[request.value.context] || []).sort();
        }

        return [];
    });

    //------------------------------------------------------------------------------------------------------------------
    // Methods
    //------------------------------------------------------------------------------------------------------------------

    function isRequestValid() : boolean
    {
        return !!request.value.context
            && !!request.value.operation
            && !!request.value.serviceGroup
            && !!request.value.payload
            && request.value.payload.trim().startsWith('{')
            && request.value.payload.trim().endsWith('}');
    }

    function handleServiceChanged(newService : string | undefined) : void
    {
        // TODO: Make this not work by queue.

        if(newService)
        {
            request.value.serviceGroup
                = serviceGroups.value.includes(newService) ? newService : (serviceGroups.value[0] ?? newService);
        }
        else
        {
            request.value.serviceGroup = undefined;
        }
    }

    function cancelRequest() : void
    {
        requestMan.cancelRequest();
    }

    function refreshServiceList() : void
    {
        if(!servicesLoading.value)
        {
            requestMan.refreshServiceList(currentEnv.value?.id);
        }
    }

    function startAutoRefreshServices() : void
    {
        refreshTimer.value = setInterval(refreshServiceList, refreshInterval);
    }

    function stopAutoRefreshServices() : void
    {
        clearInterval(refreshTimer.value);
    }

    function reformatPayload() : void
    {
        requestMan.reformat();
    }

    function resetForm() : void
    {
        requestMan.reset();
        handleServiceChanged(selectedServiceGroup.value);
    }

    async function sendRequest() : Promise<void>
    {
        requestMan.reformat();
        await requestMan.sendRequest();
    }

    function onClearService() : void
    {
        request.value.serviceGroup = undefined;
        request.value.context = undefined;
        request.value.operation = undefined;
    }

    function onClearContext() : void
    {
        request.value.operation = undefined;
    }

    //------------------------------------------------------------------------------------------------------------------
    // Watchers
    //------------------------------------------------------------------------------------------------------------------

    watch(request.value, () =>
    {
        if(!contexts.value.includes(request.value.context))
        {
            request.value.context = undefined;
        }
    });

    watch(request.value, () =>
    {
        if(!operations.value.includes(request.value.operation))
        {
            request.value.operation = undefined;
        }
    });

    watch(selectedServiceGroup, (newValue) =>
    {
        handleServiceChanged(newValue);
    });

    //------------------------------------------------------------------------------------------------------------------
    // Lifecycle Hooks
    //------------------------------------------------------------------------------------------------------------------

    onMounted(() =>
    {
        startAutoRefreshServices();
    });

    onBeforeUnmount(() =>
    {
        stopAutoRefreshServices();
    });
</script>

<!--------------------------------------------------------------------------------------------------------------------->
