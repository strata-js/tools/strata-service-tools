// ---------------------------------------------------------------------------------------------------------------------
// Navigation Guards
// ---------------------------------------------------------------------------------------------------------------------

import {
    NavigationGuard,
    NavigationGuardNext,
    NavigationGuardReturn, RouteLocationNormalizedGeneric,
    RouteLocationNormalizedLoadedGeneric,
    _Awaitable,
} from 'vue-router';

// Managers
import authMan from './managers/auth.js';

// ---------------------------------------------------------------------------------------------------------------------

type AuthenticationState = 'logged in' | 'logged out';

// ---------------------------------------------------------------------------------------------------------------------

export function ensureAuthenticationState(state : AuthenticationState) : NavigationGuard
{
    return function(
        _from : RouteLocationNormalizedLoadedGeneric,
        _to : RouteLocationNormalizedGeneric,
        next : NavigationGuardNext
    ) : _Awaitable<NavigationGuardReturn>
    {
        let shouldNavigate : false | undefined = undefined;

        if(authMan.initialized && authMan.authEnabled)
        {
            if(state === 'logged in' && !authMan.account)
            {
                shouldNavigate = false;
            }
            else if(state === 'logged out' && authMan.account)
            {
                shouldNavigate = false;
            }
        }

        return next(shouldNavigate);
    };
}

// ---------------------------------------------------------------------------------------------------------------------
