//----------------------------------------------------------------------------------------------------------------------
// PluginManager
//----------------------------------------------------------------------------------------------------------------------

import { App } from 'vue';
import { BehaviorSubject, Observable } from 'rxjs';

// Interfaces
import { SSTClientConfig, SSTPluginStorage, SSTPluginStorageConfig } from '../../../common/interfaces/plugins';
import { SSTClientRequest } from '../../../common/interfaces/request';

// Managers
import authMan from '../managers/auth.js';
import envMan from '../managers/environments.js';

// Resource Access
import pluginRA from '../resource-access/plugin';
import sioRA from '../resource-access/sio';

//----------------------------------------------------------------------------------------------------------------------

export type SSTClientConfigPopulated = SSTClientConfig & {
    plugin : string,
    systemStorage ?: SSTPluginStorageConfig,
    userStorage ?: SSTPluginStorageConfig
};

/* eslint-disable no-await-in-loop */

class PluginManager
{
    #configsSubject : BehaviorSubject<SSTClientConfigPopulated[]>
        = new BehaviorSubject<SSTClientConfigPopulated[]>([]);

    //------------------------------------------------------------------------------------------------------------------

    get configs$() : Observable<SSTClientConfigPopulated[]> { return this.#configsSubject.asObservable(); }

    //------------------------------------------------------------------------------------------------------------------

    get configs() : SSTClientConfigPopulated[] { return this.#configsSubject.getValue(); }

    //------------------------------------------------------------------------------------------------------------------

    async init(app : App) : Promise<void>
    {
        const pluginModules = [];

        // Registering all plugins
        const plugins = await pluginRA.list();
        for(const plugin of plugins)
        {
            console.info(`Registering plugin: ${ plugin }...`);
            pluginModules.push({ name: plugin, module: await pluginRA.load(plugin) });
        }

        console.info('All plugins registered.');
        console.info('Loading registered client plugins...');

        for(const plugin of pluginModules)
        {
            console.info(`Loading plugin: ${ plugin.name }...`);
            if(plugin.module.registerClient)
            {
                try
                {
                    const config = await plugin.module.registerClient(app, sioRA);
                    if(config)
                    {
                        config.plugin = plugin.name;
                        this.#configsSubject.next([ ...this.configs, config ]);
                    }
                }
                catch (ex)
                {
                    console.error(`Failed to register plugin: ${ plugin.name }:`, ex);
                }

                console.info(`Successfully registered plugin: ${ plugin.name }.`);
            }
            else
            {
                console.warn(`Plugin does not have a client registration function: ${ plugin.name }.`);
            }
        }

        await this.loadStorages();

        if(this.configs.filter((config) => config.auth).length > 1)
        {
            console.warn(`Multiple plugins detected with 'auth' components.`);
        }
    }

    async loadStorages() : Promise<void>
    {
        const pluginStorages = await pluginRA.listStorage();

        this.configs?.forEach((config) =>
        {
            config.systemStorage = pluginStorages.find((storage) => storage.plugin === config.plugin
                && (storage.accountId === undefined || storage.accountId === null || storage.accountId === -1))
                ?.storage;

            if(authMan.authEnabled)
            {
                config.userStorage = pluginStorages.find((storage) => storage.plugin === config.plugin
                    && storage.accountId === authMan.account?.id)?.storage;
            }

            this.handleConfigDefaults(config);
        });

        this.#configsSubject.next([ ...this.configs ]);
    }

    handleConfigDefaults(config : SSTClientConfigPopulated) : void
    {
        if(config.systemStorage)
        {
            config.systemStorage = this.getStorageDefaults(config, config.systemStorage);
        }

        if(authMan.authEnabled && config.userStorage)
        {
            config.userStorage = this.getStorageDefaults(config, config.userStorage);
        }
    }

    getStorageDefaults(config : SSTClientConfig, storage ?: SSTPluginStorageConfig) : SSTPluginStorageConfig
    {
        const newStorage = {
            ...config.defaults?.base,
            ...storage,
        };

        // Add & set env defaults
        newStorage.environments = newStorage.environments ?? {};
        envMan.environments.forEach((env) =>
        {
            const envConfig = newStorage.environments[env.id];
            newStorage.environments[env.id] = {
                ...config.defaults?.environment,
                ...envConfig,
            };
        });

        // Remove env entries that don't exist
        const envKeys = Object.keys(newStorage.environments);
        envKeys?.forEach((key) =>
        {
            if(!envMan.environments.some((env) => env.id === key))
            {
                // eslint-disable-next-line @typescript-eslint/no-dynamic-delete
                delete newStorage.environments[key];
            }
        });

        return newStorage;
    }

    async addUpdateStore(record : SSTPluginStorage) : Promise<SSTPluginStorage>
    {
        let newRecord : SSTPluginStorage | null = null;

        if(await pluginRA.storageExists(record.plugin, record.accountId))
        {
            await pluginRA.updateStorage(record);
            newRecord = await pluginRA.getStorage(record.plugin, record.accountId);
        }
        else
        {
            newRecord = await pluginRA.addStorage(record);
        }

        return newRecord;
    }

    async removeStorage(pluginName : string, accountId ?: number) : Promise<void>
    {
        if(await pluginRA.storageExists(pluginName, accountId))
        {
            await pluginRA.deleteStorage(pluginName, accountId);
        }
    }

    async beforeSendRequest(request : SSTClientRequest) : Promise<void>
    {
        for(const config of this.configs)
        {
            if(typeof config.beforeSendRequest === 'function')
            {
                await config.beforeSendRequest(
                    request,
                    {
                        id: envMan.currentEnv.id,
                        options: {
                            system: config.systemStorage,
                            user: config.userStorage,
                        },
                    }
                );
            }
        }
    }

    async onEnvironmentAdded(envId : string) : Promise<void>
    {
        for(const config of this.configs)
        {
            if(typeof config.onEnvironmentAdded === 'function')
            {
                await config.onEnvironmentAdded(
                    {
                        id: envId,
                        options: {
                            system: config.systemStorage,
                            user: config.userStorage,
                        },
                    }
                );
            }
        }
    }

    async onEnvironmentDeleted(envId : string) : Promise<void>
    {
        for(const config of this.configs)
        {
            if(typeof config.onEnvironmentDeleted === 'function')
            {
                await config.onEnvironmentDeleted(
                    {
                        id: envId,
                        options: {
                            system: config.systemStorage,
                            user: config.userStorage,
                        },
                    }
                );
            }
        }
    }

    async onEnvironmentUpdated(envId : string) : Promise<void>
    {
        for(const config of this.configs)
        {
            if(typeof config.onEnvironmentUpdated === 'function')
            {
                await config.onEnvironmentUpdated(
                    {
                        id: envId,
                        options: {
                            system: config.systemStorage,
                            user: config.userStorage,
                        },
                    }
                );
            }
        }
    }
}

//----------------------------------------------------------------------------------------------------------------------

export default new PluginManager();

//----------------------------------------------------------------------------------------------------------------------
