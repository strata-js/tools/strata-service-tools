//----------------------------------------------------------------------------------------------------------------------
// RequestManager
//----------------------------------------------------------------------------------------------------------------------

import JSON5 from 'json5';
import { BehaviorSubject, Observable } from 'rxjs';
import { DiscoveredServices, ResponseEnvelope } from '@strata-js/strata';

// Interfaces
import { SSTClientRequest } from '../../../common/interfaces/request.js';

// Managers
import envMan from './environments.js';
import pluginMan from './plugin.js';

// Resource Access
import sioRA from '../resource-access/sio.js';

//----------------------------------------------------------------------------------------------------------------------

class RequestManager
{
    private requestResolver : (value : unknown) => void = Promise.resolve;

    // Subjects
    #requestSubject : BehaviorSubject<Partial<SSTClientRequest>>
        = new BehaviorSubject<Partial<SSTClientRequest>>(
            { context: '', operation: '', payload: '{\n}', serviceGroup: '', auth: '' }
        );

    #responseSubject : BehaviorSubject<ResponseEnvelope | undefined>
        = new BehaviorSubject<ResponseEnvelope | undefined>(undefined);

    #responseLoadingSubject : BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    #requestDurationSubject : BehaviorSubject<number | undefined> = new BehaviorSubject<number | undefined>(undefined);
    #responseOutstanding : BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    #services : BehaviorSubject<DiscoveredServices | undefined>
        = new BehaviorSubject<DiscoveredServices | undefined>(undefined);

    #servicesLoading : BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

    //------------------------------------------------------------------------------------------------------------------
    // Properties
    //------------------------------------------------------------------------------------------------------------------

    get request$() : Observable<Partial<SSTClientRequest>> { return this.#requestSubject.asObservable(); }
    get response$() : Observable<ResponseEnvelope | undefined> { return this.#responseSubject.asObservable(); }
    get responseLoading$() : Observable<boolean> { return this.#responseLoadingSubject.asObservable(); }
    get requestDuration$() : Observable<number | undefined> { return this.#requestDurationSubject.asObservable(); }
    get responseOutstanding$() : Observable<boolean> { return this.#responseOutstanding.asObservable(); }
    get services$() : Observable<DiscoveredServices | undefined> { return this.#services.asObservable(); }
    get servicesLoading$() : Observable<boolean> { return this.#servicesLoading.asObservable(); }

    get request() : Partial<SSTClientRequest> { return this.#requestSubject.getValue(); }
    get response() : Partial<ResponseEnvelope | undefined> { return this.#responseSubject.getValue(); }

    //------------------------------------------------------------------------------------------------------------------

    async init() : Promise<void>
    {
        // Subscribe to currentEnv changes
        envMan.currentEnv$.subscribe((currentEnv) =>
        {
            this.refreshServiceList(currentEnv?.id);
        });

        this.refreshServiceList(envMan.currentEnv?.id);
    }

    format(payload ?: string) : string
    {
        const obj = JSON5.parse(payload ?? '{}');
        const formatted = JSON5.stringify(obj, null, 4);
        if(formatted === '{}')
        {
            return '{\n}';
        }
        return formatted;
    }

    reformat() : void
    {
        this.request.payload = this.format(this.request.payload);
        this.#requestSubject.next({ ...this.request });
    }

    reset() : void
    {
        this.#requestSubject.next({ context: '', operation: '', payload: '{\n}', serviceGroup: '' });
    }

    async refreshServiceList(envName ?: string) : Promise<void>
    {
        if(envName)
        {
            // Start by just getting the list of services, this will get any updated V1 and all V2 services.
            this.#servicesLoading.next(true);
            const services : DiscoveredServices = await sioRA.listServices(envName)
                .catch((error) => { console.error(error); return {}; });

            this.#services.next(services);
            this.#servicesLoading.next(false);
        }
        else
        {
            this.#services.next(undefined);
        }
    }

    async sendRequest() : Promise<void>
    {
        if(envMan.currentEnv)
        {
            this.#responseOutstanding.next(true);
            await pluginMan.beforeSendRequest(this.request as SSTClientRequest)
                .finally(() =>
                {
                    this.#responseOutstanding.next(false);
                });

            // Clear response subjects
            this.#requestDurationSubject.next(undefined);
            this.#responseSubject.next(undefined);
            this.#responseLoadingSubject.next(true);

            const context = this.request.context ?? '';
            const operation = this.request.operation ?? '';
            const payload = this.request.payload ?? '{\n}';
            const serviceGroup = this.request.serviceGroup ?? '';

            const envelope : SSTClientRequest = {
                context,
                operation,
                payload,
                serviceGroup,
                auth: this.request.auth,
                env: envMan.currentEnv.id,
            };

            const reqStart = performance.now();
            // eslint-disable-next-line no-async-promise-executor
            const response = await new Promise(async(resolve, reject) =>
            {
                this.#responseOutstanding.next(true);
                this.requestResolver = resolve;
                try
                {
                    const resp = await sioRA.request(envelope);
                    resolve(resp);
                }
                catch (error)
                {
                    reject(error);
                }
            })
                .finally(() =>
                {
                    this.#responseOutstanding.next(false);
                });
            const reqEnd = performance.now();

            // Store the duration, as seen by the client
            this.#requestDurationSubject.next(reqEnd - reqStart);

            // Store response
            this.#responseSubject.next(response as unknown as ResponseEnvelope);

            // Turn off loading
            this.#responseLoadingSubject.next(false);
        }
        else
        {
            console.error('Cannot send request without an environment.');
        }
    }

    cancelRequest() : void
    {
        this.#responseOutstanding.next(false);
        this.requestResolver(undefined);
    }
}

//----------------------------------------------------------------------------------------------------------------------

export default new RequestManager();

//----------------------------------------------------------------------------------------------------------------------
