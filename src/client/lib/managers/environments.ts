//----------------------------------------------------------------------------------------------------------------------
// EnvironmentsManager
//----------------------------------------------------------------------------------------------------------------------

import { BehaviorSubject, Observable } from 'rxjs';

// Managers
import authMan from './auth.js';

// Models
import { Environment, EnvironmentOptions } from '../models/environment.js';

// Resource Access
import envRA from '../resource-access/environment.js';

//----------------------------------------------------------------------------------------------------------------------

class EnvironmentsManager
{
    #environmentsSubject : BehaviorSubject<Environment[]> = new BehaviorSubject<Environment[]>([]);
    #currentEnvSubject : BehaviorSubject<Environment | undefined>
        = new BehaviorSubject<Environment | undefined>(undefined);

    //------------------------------------------------------------------------------------------------------------------
    // Properties
    //------------------------------------------------------------------------------------------------------------------

    get environments$() : Observable<Environment[]> { return this.#environmentsSubject.asObservable(); }
    get currentEnv$() : Observable<Environment | undefined> { return this.#currentEnvSubject.asObservable(); }

    get environments() : Environment[] { return this.#environmentsSubject.getValue(); }
    get currentEnv() : Environment | undefined { return this.#currentEnvSubject.getValue(); }

    //------------------------------------------------------------------------------------------------------------------

    $onAccountHandler() : void
    {
        this.refresh();
    }

    //------------------------------------------------------------------------------------------------------------------

    async init() : Promise<void>
    {
        // Subscribe to changes in the account
        authMan.account$.subscribe(this.$onAccountHandler.bind(this));

        // Refresh our list of environments
        await this.refresh();
    }

    async refresh() : Promise<void>
    {
        this.#environmentsSubject.next(await envRA.list());

        if(!this.currentEnv || !this.environments.includes(this.currentEnv))
        {
            this.#currentEnvSubject.next(this.environments[0]);
        }
    }

    selectEnv(id ?: string) : void
    {
        if(id)
        {
            const env = this.environments.find((envItem) => envItem.id === id);
            if(env)
            {
                this.#currentEnvSubject.next(env);
            }
        }
        else
        {
            this.#currentEnvSubject.next(undefined);
        }
    }

    async add(envDef : EnvironmentOptions) : Promise<Environment>
    {
        const env = await envRA.add(envDef);
        await this.refresh();

        return env;
    }

    async update(envDef : EnvironmentOptions) : Promise<Environment>
    {
        const env = await envRA.update(envDef);
        await this.refresh();

        return env;
    }

    async remove(envID : string) : Promise<void>
    {
        await envRA.remove(envID);
        await this.refresh();
    }
}

//----------------------------------------------------------------------------------------------------------------------

export default new EnvironmentsManager();

//----------------------------------------------------------------------------------------------------------------------
