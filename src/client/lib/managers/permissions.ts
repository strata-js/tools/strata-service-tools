// ---------------------------------------------------------------------------------------------------------------------
// PermissionsManager
//----------------------------------------------------------------------------------------------------------------------

import tp from 'trivialperms';

// Models
import { Account } from '../models/account.js';

// Resource Access
import rolesRA from '../resource-access/roles.js';

//----------------------------------------------------------------------------------------------------------------------

export async function init() : Promise<void>
{
    const roles = (await rolesRA.list()) ?? [];
    tp.loadGroups(roles);
}

export function hasPerm(user : Account, perm : string) : boolean
{
    return tp.hasPerm(user, perm);
}

export function hasGroup(user : Account, groupName : string) : boolean
{
    return tp.hasGroup(user, groupName);
}

// ---------------------------------------------------------------------------------------------------------------------
