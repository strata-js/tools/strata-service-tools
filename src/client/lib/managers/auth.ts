//----------------------------------------------------------------------------------------------------------------------
// AuthManager
//----------------------------------------------------------------------------------------------------------------------

import { BehaviorSubject, Observable } from 'rxjs';

// Models
import { Account } from '../models/account.js';

// Resource Access
import accountRA from '../resource-access/account.js';

//----------------------------------------------------------------------------------------------------------------------

class AuthManager
{
    #accountSubject : BehaviorSubject<Account | undefined> = new BehaviorSubject<Account | undefined>(undefined);
    #authEnabledSubject : BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);
    #initializedSubject : BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

    //------------------------------------------------------------------------------------------------------------------
    // Properties
    //------------------------------------------------------------------------------------------------------------------

    get account$() : Observable<Account | undefined> { return this.#accountSubject.asObservable(); }
    get authEnabled$() : Observable<boolean> { return this.#authEnabledSubject.asObservable(); }
    get initialized$() : Observable<boolean> { return this.#initializedSubject.asObservable(); }

    get account() : Account | undefined { return this.#accountSubject.getValue(); }
    get authEnabled() : boolean { return this.#authEnabledSubject.getValue(); }
    get initialized() : boolean { return this.#initializedSubject.getValue(); }

    //------------------------------------------------------------------------------------------------------------------

    async init() : Promise<void>
    {
        const account = await accountRA.getCurrent()
            .catch((error) =>
            {
                if(error.response?.status === 418)
                {
                    this.#authEnabledSubject.next(false);
                }

                return undefined;
            });

        this.#accountSubject.next(account);
        this.#initializedSubject.next(true);
    }

    async logout() : Promise<void>
    {
        await accountRA.logout();
        this.#accountSubject.next(undefined);
    }
}

//----------------------------------------------------------------------------------------------------------------------

export default new AuthManager();

//----------------------------------------------------------------------------------------------------------------------
