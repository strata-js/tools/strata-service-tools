//----------------------------------------------------------------------------------------------------------------------
// ClipboardManager
//----------------------------------------------------------------------------------------------------------------------

class ClipboardManager
{
    async $checkWritePermissions() : Promise<boolean>
    {
        // TODO: Have to do weird cast shenanigans, because typescript's wrong about the possible values for `name`.
        const result = await navigator.permissions.query({ name: 'clipboard-write' as unknown as PermissionName });
        return result.state == 'granted' || result.state == 'prompt';
    }

    async write(text : string) : Promise<void>
    {
        if(await this.$checkWritePermissions())
        {
            return navigator.clipboard.writeText(text);
        }
        else
        {
            alert('Clipboard API does not support write on your platform, or permission denied.');
        }
    }

    async read() : Promise<string>
    {
        return navigator.clipboard.readText();
    }
}

//----------------------------------------------------------------------------------------------------------------------

export default new ClipboardManager();

//----------------------------------------------------------------------------------------------------------------------
