//----------------------------------------------------------------------------------------------------------------------
// Environment
//----------------------------------------------------------------------------------------------------------------------

import { shortID } from '../../../common/utils/misc.js';

//----------------------------------------------------------------------------------------------------------------------

export interface EnvironmentOptions
{
    id ?: string;
    name : string;
    host : string;
    port : number;
    db ?: number;
    backend : string;
    username ?: string;
    password ?: string;
    secure : boolean;
    accountID ?: number | null;
}

export interface EnvironmentConnectionStatus
{
    envId : string;
    status : 'connecting' | 'connected' | 'disconnected';
    error ?: string;
}

//----------------------------------------------------------------------------------------------------------------------

export class Environment
{
    public id : string;
    public name : string;
    public host : string;
    public port : number;
    public db : number;
    public backend : string;
    public username ?: string;
    public password ?: string;
    public secure : boolean;
    public accountID ?: number | null;

    constructor(options : EnvironmentOptions)
    {
        this.id = options.id ?? shortID();
        this.name = options.name;
        this.host = options.host;
        this.port = options.port;
        this.db = options.db ?? 0;
        this.backend = options.backend;
        this.username = options.username;
        this.password = options.password;
        this.secure = options.secure;
        this.accountID = options.accountID;
    }

    //------------------------------------------------------------------------------------------------------------------
    // Model API
    //------------------------------------------------------------------------------------------------------------------

    public update(options : Partial<EnvironmentOptions>) : void
    {
        Object.assign(this, options);
    }

    //------------------------------------------------------------------------------------------------------------------
    // Serialization
    //------------------------------------------------------------------------------------------------------------------

    public toJSON() : Record<string, unknown>
    {
        return {
            id: this.id,
            name: this.name,
            host: this.host,
            port: this.port,
            db: this.db,
            backend: this.backend,
            username: this.username,
            password: this.password,
            secure: this.secure,
            accountID: this.accountID,
        };
    }

    //------------------------------------------------------------------------------------------------------------------
    // Deserialization
    //------------------------------------------------------------------------------------------------------------------

    static fromJSON(jsonObj : EnvironmentOptions) : Environment
    {
        return new Environment(jsonObj);
    }
}

//----------------------------------------------------------------------------------------------------------------------
