//----------------------------------------------------------------------------------------------------------------------
// Account
//----------------------------------------------------------------------------------------------------------------------

import { AccountOptions, AccountSettings } from '../../../common/interfaces/account.js';

//----------------------------------------------------------------------------------------------------------------------

export class Account
{
    public readonly id : number;
    public readonly email : string = '';

    public name = '';
    public avatar = '';
    public permissions : string[] = [];
    public groups : string[] = [];
    public settings : AccountSettings = {};

    constructor(options : AccountOptions)
    {
        this.id = options.id;
        this.email = options.email;
        this.name = options.name;
        this.avatar = options.avatar || '';
        this.permissions = options.permissions ?? [];
        this.groups = options.groups ?? [];
        this.settings = options.settings ?? {};
    }

    //------------------------------------------------------------------------------------------------------------------
    // Model API
    //------------------------------------------------------------------------------------------------------------------

    public update(options : Partial<AccountOptions>) : void
    {
        Object.assign(this, options);
    }

    //------------------------------------------------------------------------------------------------------------------
    // Serialization
    //------------------------------------------------------------------------------------------------------------------

    public toJSON() : AccountOptions
    {
        return {
            id: this.id,
            email: this.email,
            name: this.name,
            avatar: this.avatar,
            permissions: this.permissions,
            groups: this.groups,
            settings: this.settings,
        };
    }

    //------------------------------------------------------------------------------------------------------------------
    // Deserialization
    //------------------------------------------------------------------------------------------------------------------

    static fromJSON(jsonObj : AccountOptions) : Account
    {
        // FIXME: This bypasses all the checks of TypeScript! We should use AJV to do the validation here.
        return new Account(jsonObj as unknown as AccountOptions);
    }
}

//----------------------------------------------------------------------------------------------------------------------
