//----------------------------------------------------------------------------------------------------------------------
// Role
//----------------------------------------------------------------------------------------------------------------------

import { RoleOptions } from '../../../common/interfaces/role.js';
import { AccountOptions } from '../../../common/interfaces/account.js';

//----------------------------------------------------------------------------------------------------------------------

export class Role
{
    public readonly id : number;
    public readonly name : string;
    public permissions : string[] = [];

    constructor(options : RoleOptions)
    {
        this.id = options.id;
        this.name = options.name;
        this.permissions = options.permissions ?? [];
    }

    //------------------------------------------------------------------------------------------------------------------
    // Model API
    //------------------------------------------------------------------------------------------------------------------

    public update(options : Partial<AccountOptions>) : void
    {
        Object.assign(this, options);
    }

    //------------------------------------------------------------------------------------------------------------------
    // Serialization
    //------------------------------------------------------------------------------------------------------------------

    public toJSON() : Record<string, unknown>
    {
        return {
            id: this.id,
            name: this.name,
            permissions: this.permissions,
        };
    }

    //------------------------------------------------------------------------------------------------------------------
    // Deserialization
    //------------------------------------------------------------------------------------------------------------------

    static fromJSON(jsonObj : RoleOptions) : Role
    {
        // FIXME: This bypasses all the checks of TypeScript! We should use AJV to do the validation here.
        return new Role(jsonObj);
    }
}

//----------------------------------------------------------------------------------------------------------------------
