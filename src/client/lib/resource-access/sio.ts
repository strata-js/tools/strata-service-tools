//----------------------------------------------------------------------------------------------------------------------
// Socket.io Client Resource Access
//----------------------------------------------------------------------------------------------------------------------

import { Socket, io } from 'socket.io-client';
import { BehaviorSubject, Observable } from 'rxjs';

import { DiscoveredServices } from '@strata-js/strata';

// Interfaces
import { SSTClientRequest } from '../../../common/interfaces/request.js';
import { EnvironmentConnectionStatus } from '../models/environment.js';

//----------------------------------------------------------------------------------------------------------------------

class SocketIOResourceAccess
{
    #socket ?: Socket;

    #connectionStatus : BehaviorSubject<EnvironmentConnectionStatus[]>
        = new BehaviorSubject<EnvironmentConnectionStatus[]>([]);

    get connectionStatus$() : Observable<EnvironmentConnectionStatus[]>
    {
        return this.#connectionStatus.asObservable();
    }

    get connectionStatus() : EnvironmentConnectionStatus[] { return this.#connectionStatus.getValue(); }

    public init(socketURL = '/') : void
    {
        this.#socket = io(socketURL);

        this.#socket.on('connect', () =>
        {
            console.info('socket.io connected.');
        });

        this.#socket.on('envConnectionStatus', (status : EnvironmentConnectionStatus[]) =>
        {
            this.#connectionStatus.next(status);
        });
    }

    public async request(request : SSTClientRequest) : Promise<Record<string, unknown>>
    {
        return new Promise((resolve) =>
        {
            if(this.#socket)
            {
                this.#socket.emit('request', request, (response) =>
                {
                    resolve(response);
                });
            }
        });
    }

    public listServices(env : string) : Promise<DiscoveredServices>
    {
        return new Promise((resolve) =>
        {
            if(this.#socket)
            {
                this.#socket.emit('listServices', { env }, (response) =>
                {
                    resolve(response);
                });
            }
        });
    }
}

//----------------------------------------------------------------------------------------------------------------------

export default new SocketIOResourceAccess();

//----------------------------------------------------------------------------------------------------------------------
