//----------------------------------------------------------------------------------------------------------------------
// EnvironmentResourceAccess
//----------------------------------------------------------------------------------------------------------------------

import axios from 'axios';

// Interfaces
import { EnvironmentFilters } from '../../../common/interfaces/filters.js';

// Models
import { Environment, EnvironmentOptions } from '../models/environment.js';

//----------------------------------------------------------------------------------------------------------------------

class EnvironmentResourceAccess
{
    #envCache = new Map<string, Environment>();

    $buildOrUpdate(env : EnvironmentOptions) : Environment
    {
        let model = this.#envCache.get(env.id);
        if(model)
        {
            model.update(env);
        }
        else
        {
            model = Environment.fromJSON(env);
            this.#envCache.set(env.id, model);
        }

        return model;
    }

    async list(filters ?: EnvironmentFilters) : Promise<Environment[]>
    {
        const { data } = await axios.get('/api/environments', { params: filters });
        return data.map(this.$buildOrUpdate.bind(this));
    }

    async add(env : EnvironmentOptions) : Promise<Environment>
    {
        const { data } = await axios.post('/api/environments', env);
        return this.$buildOrUpdate(data);
    }

    async update(env : EnvironmentOptions) : Promise<Environment>
    {
        const { data } = await axios.patch(`/api/environments/${ env.id }`, env);
        return this.$buildOrUpdate(data);
    }

    async remove(envID : string) : Promise<void>
    {
        await axios.delete(`/api/environments/${ envID }`);
        this.#envCache.delete(envID);
    }
}

//----------------------------------------------------------------------------------------------------------------------

export default new EnvironmentResourceAccess();

//----------------------------------------------------------------------------------------------------------------------
