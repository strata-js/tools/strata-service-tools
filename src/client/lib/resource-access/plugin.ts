//----------------------------------------------------------------------------------------------------------------------
// Plugin Resource Access
//----------------------------------------------------------------------------------------------------------------------

import axios from 'axios';

// Interfaces
import { SSTClientPlugin, SSTPluginStorage } from '../../../common/interfaces/plugins';

//----------------------------------------------------------------------------------------------------------------------

class PluginResourceAccess
{
    async list() : Promise<string[]>
    {
        const response = await axios.get('/api/plugins');
        return response.data;
    }

    async load(name : string) : Promise<SSTClientPlugin>
    {
        try
        {
            const pluginModule = await import(/* @vite-ignore */ `/api/plugins/load/${ name }`);
            if(pluginModule)
            {
                console.debug(`Successfully loaded plugin: ${ name }`);
                return pluginModule;
            }
        }
        catch (ex)
        {
            console.warn(`Failed to load plugin: ${ name }:`, ex);
        }

        console.warn(`Failed to load plugin: ${ name }.`);
    }

    async listStorage() : Promise<SSTPluginStorage[]>
    {
        const { data } = await axios.get('/api/plugins/storage');
        return data;
    }

    async getStorage(name : string, accountId ?: number) : Promise<SSTPluginStorage>
    {
        const { data } = await axios.get(`/api/plugins/storage/${ name }`, { params: { accountId } });
        return data;
    }

    async storageExists(name : string, accountId ?: number) : Promise<boolean>
    {
        const { data } = await axios.get(`/api/plugins/storage/exists/${ name }`, { params: { accountId } });
        return data;
    }

    async addStorage(record : SSTPluginStorage) : Promise<SSTPluginStorage>
    {
        const { data } = await axios.post('/api/plugins/storage', record);
        return data;
    }

    async updateStorage(record : SSTPluginStorage) : Promise<void>
    {
        await axios.patch('/api/plugins/storage', record);
    }

    async deleteStorage(name : string, accountId ?: number) : Promise<void>
    {
        await axios.delete(`/api/plugins/storage/${ name }`, { params: { accountId } });
    }
}

//----------------------------------------------------------------------------------------------------------------------

export default new PluginResourceAccess();

//----------------------------------------------------------------------------------------------------------------------
