//----------------------------------------------------------------------------------------------------------------------
// ColorMode
//----------------------------------------------------------------------------------------------------------------------

import { BehaviorSubject, Observable } from 'rxjs';

//----------------------------------------------------------------------------------------------------------------------

type ValidColorMode = 'auto' | 'dark' | 'light';
type ValidBSTheme = 'dark' | 'light';

//----------------------------------------------------------------------------------------------------------------------

export class ColorMode
{
    #currentModeSubject = new BehaviorSubject<ValidColorMode>('auto');
    #currentBSTheme = new BehaviorSubject<ValidBSTheme>('dark');

    //------------------------------------------------------------------------------------------------------------------
    // Observables
    //------------------------------------------------------------------------------------------------------------------

    get colorMode$() : Observable<ValidColorMode>
    {
        return this.#currentModeSubject.asObservable();
    }

    get bsTheme$() : Observable<ValidBSTheme>
    {
        return this.#currentBSTheme.asObservable();
    }

    //------------------------------------------------------------------------------------------------------------------
    // Properties
    //------------------------------------------------------------------------------------------------------------------

    get colorMode() : ValidColorMode
    {
        return this.#currentModeSubject.getValue();
    }

    get bsTheme() : ValidBSTheme
    {
        return this.#currentBSTheme.getValue();
    }

    //------------------------------------------------------------------------------------------------------------------

    $prefersDarkMode() : boolean
    {
        return window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches;
    }

    $calculateColorTheme() : ValidBSTheme
    {
        if(this.colorMode === 'auto')
        {
            return this.$prefersDarkMode() ? 'dark' : 'light';
        }

        return this.colorMode;
    }

    //------------------------------------------------------------------------------------------------------------------
    // Public
    //------------------------------------------------------------------------------------------------------------------

    setColorMode(mode : ValidColorMode) : void
    {
        this.#currentModeSubject.next(mode);
        this.#currentBSTheme.next(this.$calculateColorTheme());
    }
}

//----------------------------------------------------------------------------------------------------------------------

export default new ColorMode();

//----------------------------------------------------------------------------------------------------------------------
