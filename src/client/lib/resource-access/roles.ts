//----------------------------------------------------------------------------------------------------------------------
// RoleResourceAccess
//----------------------------------------------------------------------------------------------------------------------

import axios from 'axios';

// Interfaces
import { RoleOptions } from '../../../common/interfaces/role.js';

// Models
import { Role } from '../models/role.js';

//----------------------------------------------------------------------------------------------------------------------

class RoleResourceAccess
{
    #roleCache = new Map<number, Role>();

    $buildOrUpdate(role : RoleOptions) : Role
    {
        let model = this.#roleCache.get(role.id);
        if(model)
        {
            model.update(role);
        }
        else
        {
            model = Role.fromJSON(role);
            this.#roleCache.set(role.id, model);
        }

        return model;
    }

    async list() : Promise<Role[]>
    {
        const { data } = await axios.get('/api/roles');
        return data.map(this.$buildOrUpdate.bind(this));
    }
}

//----------------------------------------------------------------------------------------------------------------------

export default new RoleResourceAccess();

//----------------------------------------------------------------------------------------------------------------------
