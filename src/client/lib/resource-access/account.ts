//----------------------------------------------------------------------------------------------------------------------
// AccountResourceAccess
//----------------------------------------------------------------------------------------------------------------------

import axios from 'axios';

// Interfaces
import { AccountFilters } from '../../../common/interfaces/filters.js';
import { AccountOptions } from '../../../common/interfaces/account.js';

// Models
import { Account } from '../models/account.js';

//----------------------------------------------------------------------------------------------------------------------

class AccountResourceAccess
{
    #accountCache = new Map<number, Account>();

    $buildOrUpdate(account : AccountOptions) : Account
    {
        let model = this.#accountCache.get(account.id);
        if(model)
        {
            model.update(account);
        }
        else
        {
            model = Account.fromJSON(account);
            this.#accountCache.set(account.id, model);
        }

        return model;
    }

    async getCurrent() : Promise<Account | undefined>
    {
        const { data } = await axios.get('/auth/user');
        if(data)
        {
            return this.$buildOrUpdate(data);
        }
    }

    async list(filters ?: AccountFilters) : Promise<Account[]>
    {
        const { data } = await axios.get('/api/accounts', { params: filters });
        return data.map(this.$buildOrUpdate.bind(this));
    }

    async update(account : Record<string, unknown>) : Promise<Account>
    {
        const { data } = await axios.patch(`/api/accounts/${ account.id }`, account);
        return this.$buildOrUpdate(data);
    }

    async logout() : Promise<void>
    {
        await axios.post('/auth/logout');
    }
}

//----------------------------------------------------------------------------------------------------------------------

export default new AccountResourceAccess();

//----------------------------------------------------------------------------------------------------------------------
