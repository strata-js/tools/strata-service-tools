//----------------------------------------------------------------------------------------------------------------------
// Configuration Interfaces
//----------------------------------------------------------------------------------------------------------------------

import { Knex } from 'knex';

//----------------------------------------------------------------------------------------------------------------------

export interface LoggingConfig
{
    level : string;
    prettyPrint : boolean;
}

export interface HttpConfig
{
    host : string;
    port : number;
    secret : string;
    key : string;
    secure : boolean;
}

export interface GitLabAuthConfig
{
    baseURL : string;
    clientID : string;
    clientSecret : string;
    callbackURL : string;
}

export interface AuthConfig
{
    strategy : string;
    options : GitLabAuthConfig;
}

export interface DatabaseConfig extends Knex.Config
{
    traceQueries : boolean;
}

export interface ServerConfig
{
    logging : LoggingConfig;
    http : HttpConfig;
    auth ?: AuthConfig;
    database : DatabaseConfig;
    plugins ?: string[];
}

//----------------------------------------------------------------------------------------------------------------------

