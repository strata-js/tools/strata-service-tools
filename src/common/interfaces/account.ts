// ---------------------------------------------------------------------------------------------------------------------
// Account Interfaces
// ---------------------------------------------------------------------------------------------------------------------

// TODO: Figure out what settings will go here, eventually.
export type AccountSettings = Record<string, unknown>;

export interface AccountOptions
{
    id : number;
    email : string;
    name : string;
    avatar ?: string;
    permissions ?: string[];
    groups ?: string[];
    settings ?: AccountSettings;
}

// ---------------------------------------------------------------------------------------------------------------------
