// ---------------------------------------------------------------------------------------------------------------------
// Plugin Interface
// ---------------------------------------------------------------------------------------------------------------------

import { App, Component } from 'vue';
import { Application } from 'express';
import { SSTClientRequest } from './request.js';

// ---------------------------------------------------------------------------------------------------------------------

export interface SSTPluginConfig
{
    name : string;
    client ?: string;
    server ?: string;
}

export interface SSTPluginStorageConfig extends Record<string, any>
{
    environments ?: Record<string, any>
}

export interface SSTPluginStorage
{
    plugin : string;
    storage ?: SSTPluginStorageConfig;
    accountId ?: number;
}

export interface SSTPluginEnvironment
{
    id : string;
    options ?: {
        system ?: SSTPluginStorageConfig,
        user ?: SSTPluginStorageConfig
    }
}

export interface SSTClientConfig
{
    auth ?: Component;
    pluginConfigEditor ?: Component;
    defaults ?: {
        base ?: Record<string, any>;
        environment ?: Record<string, any>;
    }
    beforeSendRequest ?: (request : SSTClientRequest, env : SSTPluginEnvironment) => Promise<boolean>;
    onEnvironmentAdded ?: (env : SSTPluginEnvironment) => Promise<void>;
    onEnvironmentDeleted ?: (env : SSTPluginEnvironment) => Promise<void>;
    onEnvironmentUpdated ?: (env : SSTPluginEnvironment) => Promise<void>;
}

export interface SocketIOResourceAccess
{
    request(request : SSTClientRequest) : Promise<Record<string, unknown>>;
}

export interface SSTClientPlugin
{
    registerClient : (app : App, sioRA ?: SocketIOResourceAccess) => Promise<SSTClientConfig | undefined>;
}

export interface SSTServerPlugin
{
    registerServer : (app : Application) => Promise<void>;
}

// ---------------------------------------------------------------------------------------------------------------------
