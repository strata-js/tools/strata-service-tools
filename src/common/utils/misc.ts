//----------------------------------------------------------------------------------------------------------------------
// Simple utility functions
//----------------------------------------------------------------------------------------------------------------------

import _ from 'lodash';
import { customAlphabet } from 'nanoid';
import { alphanumeric } from 'nanoid-dictionary';

//----------------------------------------------------------------------------------------------------------------------

const nanoID = customAlphabet(alphanumeric, 10);

// ---------------------------------------------------------------------------------------------------------------------

/**
 * This generates nice, short ids (ex: 'HrILY', '2JjA9s') that are as unique as a uuid.
 *
 * @returns Returns a unique string id.
 */
export function shortID() : string
{
    return nanoID();
}

/**
 * Camel case all the keys in an object.
 *
 * @param obj - The object whose keys we are camel casing.
 *
 * @returns Returns a new object with the keys camel cased.
 */
export function camelCaseKeys(obj : Record<string, unknown>) : Record<string, unknown>
{
    return _.mapKeys(obj, (_val, key) =>
    {
        if(_.includes(key, '_id'))
        {
            return key.replace(/_id/g, 'ID');
        }
        else
        {
            return _.camelCase(key);
        }
    });
}

/**
 * Snake case all the keys in an object.
 *
 * @param obj - The object whose keys we are snake casing.
 *
 * @returns Returns a new object with the keys snake cased.
 */
export function snakeCaseKeys(obj : Record<string, unknown>) : Record<string, unknown>
{
    return _.mapKeys(obj, (_val, key) =>
    {
        if(_.endsWith(key, 'ID'))
        {
            return key.replace(/ID$/, '_id');
        }
        else
        {
            return _.snakeCase(key);
        }
    });
}

/**
 * A comparator function for sorting by the key of an object.
 *
 * @param key - The key to sort by.
 *
 * @returns Returns`1`, `-1`, or `0`, depending on how the object sorts.
 */
export function sortBy(key : string) : (a : Record<string, any>, b : Record<string, any>) => number
{
    return (aObj : Record<string, any>, bObj : Record<string, any>) =>
    {
        return (aObj[key] > bObj[key]) ? 1 : ((bObj[key] > aObj[key]) ? -1 : 0);
    };
}

export class PromiseTimeoutError extends Error
{
    constructor(message ?: string)
    {
        super(message ?? 'Promise timed out');
        this.name = this.constructor.name;
    }
}

/**
 * Adds a timeout to a promise.  If it does not finish before the timeout it fails.
 *
 * @param promise - The promise to run.
 * @param timeout - The time in ms to timeout the promise.
 *
 * @returns Returns the result of the promise.
 */
export function promiseTimeout(promise : Promise<any>, timeout : number, message ?: string) : Promise<any>
{
    let timer : any;
    return Promise.race([
        promise,
        new Promise((_r, rej) => timer = setTimeout(rej, timeout, new PromiseTimeoutError(message))),
    ]).finally(() => clearTimeout(timer));
}

//----------------------------------------------------------------------------------------------------------------------
