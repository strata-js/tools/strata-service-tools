# SST Plugins

There are two ways to install plugins in SST. First, you can install a module that exports the correct plugin object.
Second, you can add a directory to this directory. (That directory should contain an `index.js` file, loadable as an ES 
module.)

## Configuration

Plugins are loaded from the `plugins` section of the configuration file. This section is a list of strings, each of
which is a path to a plugin, or a module name. If the path is a module name, it will be loaded as a module. If that 
fails, it will be attempted again, but as the name of a folder in the `plugins` directory, with `index.js` appended to
the end.

Technically, you can load a plugin from anywhere on the filesystem, but it is recommended to keep them in the 
`plugins` directory.

## Plugin API

All plugins must export a `name` property (a string). They may then export a `client` and/or `server` property (also 
strings). If they do, they will be loaded as a client or server plugin, respectively.

```javascript
// Example Plugin
export const name = 'example-plugin';           // Required
export const client = './lib/client.bundle.js'; // Optional
export const server = './lib/server.js';        // Optional
```

The values for `client` and `server` should be paths to the plugin's client and server code, respectively. These paths
should be absolute paths. (Recommendation: use `path.join(import.meta.url, 'path', 'to', 'file')` to generate these 
paths.)

**NOTE:** All the client code for a plugin should be bundled into a single file. While it is possible to use 
`import()` to load files at runtimes, you will be at the whim of the browser's module loader, and whatever crazy 
path shenanigans you end up dealing with.

**NOTE:** All code must be loadable by node natively. So if you use TypeScript, for example, you must compile it to 
JavaScript in order for the plugin to load correctly.

### Module Plugins

When building a module plugin, you should export the plugin object directly from the module. This is the only way we 
will know how to load the plugin. Do this by setting the `main` property in your `package.json` to the path of the 
plugin object.

### Directory Plugins

When building a directory plugin, you should export the plugin object from an `index.js` file in the directory. This
is the only file we will look to load. The plugin object should be an ESM module.

### Adding Plugins to Docker

There are two ways to add plugins to the Docker image. First, you can add the plugin to the `plugins` directory at 
runtime by mounting a volume. Second, you can write your own dockerfile that installs the plugin through npm.

#### Mounting a Volume

To mount a volume, you can use the `-v` flag with the `docker run` command. For example:

```bash
docker run \
  -v /path/to/config:/app/config/local.yml:ro \
  -v /path/to/plugins:/app/plugins -p 9000:9000 \
  stratajs/strata-service-tools
```

This will mount the `/path/to/plugins` directory on your host machine to the `/app/plugins` directory in the 
container. This will allow you to add plugins to the container without rebuilding the image.

#### Custom Dockerfile

To create a custom Dockerfile, you can create a new file in the root of your project called `Dockerfile`. In this file,
you can use the `FROM` directive to specify the base image, and then use `RUN` to install the plugins you need. For
example:

```dockerfile
FROM stratajs/strata-service-tools

# Install the plugin through npm
RUN npm install --save my-sst-plugin

# Copy the local configuration file, modified to use the plugin
COPY ./config/local.yml /app/config/local.yml
```

Then, you can build the image with the `docker build` command:

```bash
docker build -t my-sst .
```

## Client Plugin

Client plugins are loaded in the browser. They are loaded using the `import()` function, and are expected to return 
either nothing, or a configuration object that specifies components of UI to replace. Ex:

```javascript
{
    auth: 'SomeRegisteredComponent',
}
```
